//
//  IMMusicListViewController.m
//  Isochiral Music
//
//  Created by Sripad on 23/01/14.
//  Copyright (c) 2014 MobileWays. All rights reserved.
//

#import "IMMusicListViewController.h"
#import "IMMusicDetailsViewController.h"
#import "IMLoginViewController.h"
#import "GADBannerView.h"
#import "GADRequest.h"
#import "DFPInterstitial.h"
#define Add_UnitId_Banner @"ca-app-pub-4118799173379465/6979345838"

#define CELL_BACKGROUND_RED 72.0/255.0
#define CELL_BACKGROUND_GREEN 140.0/255.0
#define CELL_BACKGROUND_BLUE 120.0/255.0

#define TABBAR_ITEM_SELECTED_TEXT_COLOR_RED 107.0/255.0
#define TABBAR_ITEM_SELECTED_TEXT_COLOR_GREEN 142.0/255.0
#define TABBAR_ITEM_SELECTED_TEXT_COLOR_BLUE 62.0/255.0

#define TABBAR_ITEM_TEXT_COLOR_RED 204.0/255.0
#define TABBAR_ITEM_TEXT_COLOR_GREEN 203.0/255.0
#define TABBAR_ITEM_TEXT_COLOR_BLUE 198.0/255.0

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

#define SEARCHBAR_HEIGHT 44
#define Ad_MobId_interstitial @"ca-app-pub-3679882451526947/5118690631"

@interface IMMusicListViewController () {
    NSString *path;
    PFObject *song;
    NSArray *alphabet;
    NSArray *numberOfRowsForEachAlphabet;
    BOOL isSearchShown;
    NSArray *searchResults;
    NSInteger adMobHeight;
}

@property (strong, nonatomic) IBOutlet UIImageView *backgroundImageView;

@end

@implementation IMMusicListViewController
@synthesize addView,interstitialAd;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.navigationItem.hidesBackButton = YES;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    alphabet = [[NSArray alloc]initWithObjects:@"#",@"A",@"B",@"C",@"D",@"E",@"F",@"G",@"H",@"I",@"J",@"K",@"L",@"M",@"N",@"O",@"P",@"Q",@"R",@"S",@"T",@"U",@"V",@"W",@"X",@"Y",@"Z",nil];
    [self setTabBarImages];
    self.musicSearch.hidden = YES;
    [IMBusinessClass sharedInstance].songsList = [[NSUserDefaults standardUserDefaults] valueForKey:@"songsList"];
    [IMBusinessClass sharedInstance].subscriptionEndDate = [[NSUserDefaults standardUserDefaults] valueForKey:@"subscriptionEndDate"];
  
    if (![PFUser currentUser]) {
        self.musicList.hidden = YES;
        if (![IMBusinessClass sharedInstance].isFromNotification) {
            IMLoginViewController *loginVC = [[IMLoginViewController alloc] initWithNibName:@"IMLoginViewController" bundle:nil];
            loginVC.delegate = self;
            double delayInSeconds = 0.1;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                self.isLoginViewShown = YES;
                [self presentViewController:loginVC animated:YES completion:nil];
            });
        }
    }
    else {
        [[PFUser currentUser] refreshInBackgroundWithBlock:^(PFObject *object, NSError *error) {
            if (object) {
                if ([IMBusinessClass sharedInstance].subscriptionEndDate !=nil) {
                    [[PFUser currentUser] setObject:[IMBusinessClass sharedInstance].subscriptionEndDate forKey:@"subscriptionEndDate"];
                    [[PFUser currentUser] saveInBackground];
                }
                else
                {
                    [[PFUser currentUser] removeObjectForKey:@"subscriptionEndDate"];
                    [[PFUser currentUser] saveInBackground];
                    
                }
            }
        }];
        UIBarButtonItem *logOut = [[UIBarButtonItem alloc] initWithTitle:@"Log Out" style:UIBarButtonItemStyleBordered target:self action:@selector(logOut:)];
        self.navigationItem.leftBarButtonItem = logOut;
        
        [self getListOfAllMusic];
    }
    self.musicSearch.hidden = YES;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        self.musicSearch.barTintColor = [UIColor colorWithRed:CELL_BACKGROUND_RED green:CELL_BACKGROUND_GREEN blue:CELL_BACKGROUND_BLUE alpha:1.0f];
        self.musicList.sectionIndexBackgroundColor = [UIColor clearColor];
        self.musicList.sectionIndexColor = [UIColor whiteColor];
    }
    
    [[UIBarButtonItem appearanceWhenContainedIn: [UISearchBar class], nil] setTintColor:[UIColor whiteColor]];
    NSDictionary *navbarTitleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                               [UIColor whiteColor],UITextAttributeTextColor, nil];
    
    [[UINavigationBar appearance] setTitleTextAttributes:navbarTitleTextAttributes];
    
    UIBarButtonItem *search = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"searchIcon"] style:UIBarButtonItemStyleBordered target:self action:@selector(showSearch:)];
    self.navigationItem.rightBarButtonItem = search;
    
    
    self.title = @"Music";
    if ([IMBusinessClass sharedInstance].isFromNotification) {
        [self.tabBarController setSelectedIndex:3];
    }
    [IMBusinessClass sharedInstance].interstialAdd = YES;
    
}
-(void)interstialAdd
{
    interstitialAd = [[GADInterstitial alloc]init];
    interstitialAd.delegate =self;
    interstitialAd.adUnitID =Ad_MobId_interstitial;
    
    [interstitialAd presentFromRootViewController:self];
    [interstitialAd loadRequest:[GADRequest request]];
//    GADRequest *r = [[GADRequest alloc] init];
//    NSArray *deviceIds = [[NSArray alloc]initWithObjects:@"71368869da062a90ee8d29e39e0213b4fe9c5531",@"dece73eea6a941972534bb9acca3ad6409d4d9de", nil];
//	r.testDevices = @[ GAD_SIMULATOR_ID ];
//    [interstitialAd loadRequest:r];
}
-(void)addAdmob
{


    self.addView = [[GADBannerView alloc]initWithAdSize:kGADAdSizeBanner origin:CGPointMake(0, VIEW_HEIGHT+10)];
    [self.view addSubview:self.addView];
    self.addView.adUnitID = Add_Unit_Id;//@"ca-app-pub-7038667452523799/9060866068";
	self.addView.rootViewController = self;
	
	GADRequest *request = [GADRequest request];

	[self.addView loadRequest:request];
 


}
-(void)viewWillAppear:(BOOL)animated {
    if ([IMBusinessClass sharedInstance].subscriptionEndDate != nil) {
        if ([[IMBusinessClass sharedInstance].subscriptionEndDate compare:[NSDate date]] == NSOrderedAscending) {
            [self addAdmob];
            adMobHeight=0;
        }
        else {
            for (UIView *subViews in self.view.subviews) {
                if([subViews isKindOfClass:[GADBannerView class]])
                {
                    [self.addView removeFromSuperview];
                    [subViews removeFromSuperview];
                    
                }
                 adMobHeight=GAD_SIZE_320x50.height+10;
                
            }
           
        }
    }
    else {
        //[self interstialAdd];
        [self addAdmob];
        adMobHeight=0;
    }
    
    if ([IMBusinessClass sharedInstance].isFromNotification) {
        [self.tabBarController setSelectedIndex:3];
    }
    
    if (![IMBusinessClass sharedInstance].songsList) {
        [IMBusinessClass sharedInstance].songsList = [[NSUserDefaults standardUserDefaults] valueForKey:@"songsList"];
        [self getListOfAllMusic];
    }
    self.musicList.hidden = NO;
    if (isSearchShown) {
        self.musicList.frame = CGRectMake(0, SEARCHBAR_HEIGHT, 320, VIEW_HEIGHT-SEARCHBAR_HEIGHT+adMobHeight);
    }
    else {
        [self performSelector:@selector(manageMusicListTableViewFrame) withObject:nil afterDelay:0.2];
    }
    [self.musicList reloadData];
    NSString *title = @"";
    if ([PFUser currentUser]) {
        title = @"Log Out";
    }
    else {
        title = @"Log in";
    }
    UIBarButtonItem *logOut = [[UIBarButtonItem alloc] initWithTitle:title style:UIBarButtonItemStyleBordered target:self action:@selector(logOut:)];
    self.navigationItem.leftBarButtonItem = logOut;
    if ([IMBusinessClass sharedInstance].subscriptionEndDate != nil && [title isEqualToString:@"Log Out"]) {
        if ([[IMBusinessClass sharedInstance].subscriptionEndDate compare:[NSDate date]] == NSOrderedAscending) {
            if([IMBusinessClass sharedInstance].interstialAdd){
                [IMBusinessClass sharedInstance].interstialAdd = NO;
                [self interstialAdd];
            }
            
        }
    }
//    else
//    {
//        [self interstialAdd];
//    }

    
}

- (void) manageMusicListTableViewFrame {
        self.musicList.frame = CGRectMake(0, 0, 320, VIEW_HEIGHT+adMobHeight);
}

-(void)getListOfAllMusic {
    [self sortObjectsForArray:[IMBusinessClass sharedInstance].songsList];
    [super showPlainViewWithFrame:CGRectMake(0, 0, 320, [UIScreen mainScreen].bounds.size.height)];
    PFQuery *query = [PFQuery queryWithClassName:@"Music"];
    [query orderByAscending:@"musicName"];
    query.limit = 999;
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            NSMutableArray *tempSongs = [[NSMutableArray alloc] init];
            for (PFObject *object in objects) {
                NSMutableDictionary *music = [[NSMutableDictionary alloc] init];
                [music setValue:[object objectForKey:@"musicName"] forKey:@"musicName"];
                [music setValue:[object objectForKey:@"musicDescription"] forKey:@"musicDescription"];
                [music setValue:[object objectForKey:@"fileURL"] forKey:@"fileURL"];
                [music setValue:[object objectForKey:@"sampleFileURL"] forKey:@"sampleFileURL"];
                [music setValue:[object objectForKey:@"imageURL"] forKey:@"imageURL"];
                [music setValue:[object objectForKey:@"duration"] forKey:@"duration"];
                [music setValue:[object objectForKey:@"status"] forKey:@"status"];
                [music setValue:object.createdAt forKey:@"createdAt"];
                [music setValue:object.updatedAt forKey:@"updatedAt"];
                [music setValue:object.objectId forKey:@"objectId"];
                [music setValue:[object objectForKey:@"freeIndicator"] forKey:@"freeIndicator"];
                
                [tempSongs addObject:music];
            }
            [super removePlainView];
            [IMBusinessClass sharedInstance].songsList = [[NSArray alloc] initWithArray:tempSongs];
            [[NSUserDefaults standardUserDefaults] setValue:[IMBusinessClass sharedInstance].songsList forKey:@"songsList"];
            [self sortObjectsForArray:[IMBusinessClass sharedInstance].songsList];
        } else {
            [super removePlainView];
            // Log details of the failure
            // [self sortObjectsForArray:[IMBusinessClass sharedInstance].songsList];
        }
        [super removePlainView];
    }];
}
-(void)userLoggedInSuccessfully:(BOOL)loggedIn {
    if ([IMBusinessClass sharedInstance].subscriptionEndDate != nil) {
        if ([[IMBusinessClass sharedInstance].subscriptionEndDate compare:[NSDate date]] == NSOrderedAscending) {
            [self interstialAdd];
        }
    }
    else
    {
        [self interstialAdd];
    }
    self.isLoginViewShown = NO;
    self.musicList.hidden = NO;
    self.musicList.frame = CGRectMake(0, 0, 320, VIEW_HEIGHT+adMobHeight);
    NSString *title = @"";
    if (loggedIn) {
        title = @"Log Out";
//        [[NSUserDefaults standardUserDefaults] setValue:[[PFUser currentUser] objectForKey:@"subscriptionEndDate"] forKey:@"subscriptionEndDate"];
//        [IMBusinessClass sharedInstance].subscriptionEndDate = [[PFUser currentUser] objectForKey:@"subscriptionEndDate"];
    }
    else {
        title = @"Log in";
    }
    UIBarButtonItem *logOut = [[UIBarButtonItem alloc] initWithTitle:title style:UIBarButtonItemStyleBordered target:self action:@selector(logOut:)];
    self.navigationItem.leftBarButtonItem = logOut;
    [self getListOfAllMusic];
    
}
-(void)logOut:(id)sender {
    if ([PFUser currentUser]) {
        [PFUser logOut];
        UIBarButtonItem *logOut = [[UIBarButtonItem alloc] initWithTitle:@"Login" style:UIBarButtonItemStyleBordered target:self action:@selector(logOut:)];
        self.navigationItem.leftBarButtonItem = logOut;
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Logout!" message:@"You are logged out" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        if ([IMBusinessClass sharedInstance].audioPlayer.isPlaying) {
            [[IMBusinessClass sharedInstance].audioPlayer stop];
        }
        
    }
    else {
        IMLoginViewController *loginVC = [[IMLoginViewController alloc] initWithNibName:@"IMLoginViewController" bundle:nil];
        loginVC.delegate = self;
        self.isLoginViewShown = YES;
        [self presentViewController:loginVC animated:YES completion:nil];
    }
}
-(void)showSearch:(id)sender {
    if (isSearchShown) {
        isSearchShown = NO;
        self.musicSearch.hidden = YES;
        [self sortObjectsForArray:[IMBusinessClass sharedInstance].songsList];
        self.musicSearch.text = @"";
        [self.musicSearch resignFirstResponder];
        [UIView animateWithDuration:0.4 animations:^{
            [self.musicList reloadData];
            self.musicList.frame = CGRectMake(0, 0, 320, VIEW_HEIGHT+adMobHeight);
        }];
    }
    else {
        [self.musicSearch becomeFirstResponder];
        isSearchShown = YES;
        self.musicSearch.hidden = NO;
        [UIView animateWithDuration:0.4 animations:^{
            self.musicList.frame = CGRectMake(0, 44, 320, VIEW_HEIGHT-SEARCHBAR_HEIGHT+adMobHeight);
        }];
    }
}
-(void)sortObjectsForArray:(NSArray *)sortArray {
    NSMutableArray *temp = [[NSMutableArray alloc] init];
    int k = 0;
    int j=0;
    for (int i = 0; i<[sortArray count]; i++) {
        if (j<[alphabet count]) {
            NSDictionary *currentSong = [sortArray objectAtIndex:i];
            if (j==0) {
                NSCharacterSet* notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
                if ([[[[currentSong objectForKey:@"musicName"] substringToIndex:1] uppercaseString] rangeOfCharacterFromSet:notDigits].location == NSNotFound)
                {
                    k++;
                }
                else {
                    [temp addObject:[NSString stringWithFormat:@"%d",k]];
                    k=0;
                    j++;
                    i--;
                }
            }
            else {
                if ([[[[currentSong objectForKey:@"musicName"] substringToIndex:1] uppercaseString] isEqualToString:[alphabet objectAtIndex:j]]) {
                    k++;
                    if (i==[sortArray count]-1) {
                        [temp addObject:[NSString stringWithFormat:@"%d",k]];
                    }
                }
                else {
                    [temp addObject:[NSString stringWithFormat:@"%d",k]];
                    k=0;
                    j++;
                    i--;
                }
            }
            
        }
    }
    int difference = ([alphabet count]-[temp count]);
    if ([temp count]!=[alphabet count]) {
        for (int z = 0; z<difference; z++) {
            [temp addObject:@"0"];
        }
    }
    numberOfRowsForEachAlphabet = [[NSArray alloc] initWithArray:temp];
    [self.musicList reloadData];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)filterSearchResults:(NSString *)searchText {
    NSMutableArray *tempSongs = [[NSMutableArray alloc] init];
    for (int i = 0; i<[[IMBusinessClass sharedInstance].songsList count]; i++) {
        NSDictionary *music = [[IMBusinessClass sharedInstance].songsList objectAtIndex:i];
        if ([[[music objectForKey:@"musicName"] uppercaseString] rangeOfString:[searchText uppercaseString]].location != NSNotFound) {
            [tempSongs addObject:[[IMBusinessClass sharedInstance].songsList objectAtIndex:i]];
        }
    }
    searchResults = [[NSArray alloc] initWithArray:tempSongs];
    [self sortObjectsForArray:searchResults];
}
#pragma mark- Table View Delegate Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [alphabet count];
}
- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    return alphabet;
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
    return [alphabet indexOfObject:title];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(10, 0, 290, 20)];
    UILabel *alphabetLbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 30, 20)];
    alphabetLbl.text = [alphabet objectAtIndex:section];
    alphabetLbl.textColor = [UIColor whiteColor];
    alphabetLbl.font = [UIFont fontWithName:@"HelveticaNeue" size:15.0f];
    alphabetLbl.backgroundColor = [UIColor clearColor];
    [headerView addSubview:alphabetLbl];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (![[numberOfRowsForEachAlphabet objectAtIndex:section] isEqualToString:@"0"]) {
        return 20;
    }
    return 0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[numberOfRowsForEachAlphabet objectAtIndex:section] intValue];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"orderQueueCell2";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    for (UIView *view in [cell.contentView subviews]) {
        [view removeFromSuperview];
    }
    int index = 0;
    for (int i = 0; i<indexPath.section; i++) {
        index += [[numberOfRowsForEachAlphabet objectAtIndex:i] intValue];
    }
    NSDictionary *songOfRow;
    if ([self.musicSearch.text isEqualToString:@""]) {
        songOfRow = [[IMBusinessClass sharedInstance].songsList objectAtIndex:index+indexPath.row];
    }
    else {
        songOfRow = [searchResults objectAtIndex:index+indexPath.row];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    UIView *backGrndView = [[UIView alloc] initWithFrame:CGRectMake(10, 5, 290, 50)];
    backGrndView.backgroundColor = [UIColor colorWithRed:CELL_BACKGROUND_RED green:CELL_BACKGROUND_GREEN blue:CELL_BACKGROUND_BLUE alpha:0.5f];
    backGrndView.layer.cornerRadius = CORNER_RADIUS;
    
    UIImageView *songPic = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
    songPic.image = [UIImage imageNamed:@"app.png"];
    [self loadImageForIndex:indexPath.row andSection:indexPath.section withMusicDetails:songOfRow];
    [backGrndView addSubview:songPic];
    UILabel *songName = [[UILabel alloc] initWithFrame:CGRectMake(60, 0, 220, 25)];
    songName.text = [songOfRow objectForKey:@"musicName"];
    songName.textColor = [UIColor whiteColor];
    songName.font = [UIFont boldSystemFontOfSize:13.0f];
    songName.backgroundColor = [UIColor clearColor];
    [backGrndView addSubview:songName];
    
    UILabel *songDuration = [[UILabel alloc] initWithFrame:CGRectMake(60, 25, 100, 25)];
    songDuration.text = [songOfRow objectForKey:@"duration"];
    songDuration.textColor = [UIColor colorWithWhite:0.6f alpha:1.0f];
    songDuration.font = [UIFont systemFontOfSize:13.0f];
    songDuration.backgroundColor = [UIColor clearColor];
    [backGrndView addSubview:songDuration];
    
    [cell.contentView addSubview:backGrndView];
    [cell.contentView setBackgroundColor:[UIColor clearColor]];
    [cell setBackgroundColor:[UIColor clearColor]];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    IMMusicDetailsViewController *musicDetailsVC = [[IMMusicDetailsViewController alloc] initWithNibName:@"IMMusicDetailsViewController" bundle:nil];
    int index = 0;
    for (int i = 0; i<indexPath.section; i++) {
        index += [[numberOfRowsForEachAlphabet objectAtIndex:i] intValue];
    }
    if ([self.musicSearch.text isEqualToString:@""]) {
        musicDetailsVC.selectedMusicDetails = [[IMBusinessClass sharedInstance].songsList objectAtIndex:index+indexPath.row];
    }
    else {
        musicDetailsVC.selectedMusicDetails = [searchResults objectAtIndex:index+indexPath.row];
    }
    [self.navigationController pushViewController:musicDetailsVC animated:YES];
}

#pragma mark - Searchbar Delegate Methods
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self filterSearchResults:searchBar.text];
    [searchBar setShowsCancelButton:YES];
    [searchBar resignFirstResponder];
    for (UIView *view in searchBar.subviews)
    {
        for (id subview in view.subviews)
        {
            if ( [subview isKindOfClass:[UIButton class]] )
            {
                [subview setEnabled:YES];
                
                return;
            }
        }
    }
    
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    if (isSearchShown) {
        [self showSearch:nil];
    }
    self.musicSearch.text = @"";
    [self sortObjectsForArray:[IMBusinessClass sharedInstance].songsList];
    [searchBar resignFirstResponder];
}
-(void)loadImageForIndex:(int)row andSection:(int)section withMusicDetails:(NSDictionary *)musicDetails {
    AFHTTPRequestOperation *postOperation = [[AFHTTPRequestOperation alloc] initWithRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[musicDetails objectForKey:@"imageURL"]]]];
    postOperation.responseSerializer = [AFImageResponseSerializer serializer];
    [postOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        UITableViewCell *cell = [self.musicList cellForRowAtIndexPath:[NSIndexPath indexPathForRow:row inSection:section]];
        for(UIView *view in [cell.contentView subviews]) {
            for (UIView *view2 in [view subviews]) {
                if ([view2 isKindOfClass:[UIImageView class]]) {
                    UIImageView *imageView = (UIImageView *)view2;
                    imageView.image = responseObject;
                }
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
    [postOperation start];
}
-(void)setTabBarImages {
    UITabBar *tabBar = self.tabBarController.tabBar;
    UITabBarItem *item0 = [tabBar.items objectAtIndex:0];
    UITabBarItem *item1 = [tabBar.items objectAtIndex:1];
    UITabBarItem *item2 = [tabBar.items objectAtIndex:2];
    UITabBarItem *item3 = [tabBar.items objectAtIndex:3];
    
    item0.title = @"Music";
    item1.title = @"Playlist";
    item2.title = @"About";
    item3.title = @"Notifications";
    if ([item0 respondsToSelector:@selector(setFinishedSelectedImage:withFinishedUnselectedImage:)]) {
        [item0 setFinishedSelectedImage:[UIImage imageNamed:@"1.png"] withFinishedUnselectedImage:[UIImage imageNamed:@"musicfiles.png"]];
    }
    if ([item1 respondsToSelector:@selector(setFinishedSelectedImage:withFinishedUnselectedImage:)]) {
        [item1 setFinishedSelectedImage:[UIImage imageNamed:@"3.png"] withFinishedUnselectedImage:[UIImage imageNamed:@"playlist.png"]];
    }
    if ([item2 respondsToSelector:@selector(setFinishedSelectedImage:withFinishedUnselectedImage:)]) {
        [item2 setFinishedSelectedImage:[UIImage imageNamed:@"2.png"] withFinishedUnselectedImage:[UIImage imageNamed:@"about.png"]];
    }
    if ([item3 respondsToSelector:@selector(setFinishedSelectedImage:withFinishedUnselectedImage:)]) {
        [item3 setFinishedSelectedImage:[UIImage imageNamed:@"notification.png"] withFinishedUnselectedImage:[UIImage imageNamed:@"4.png"]];
    }
    [item0 setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"HelveticaNeue" size:10.0f], NSFontAttributeName,  [UIColor colorWithRed:TABBAR_ITEM_TEXT_COLOR_RED green:TABBAR_ITEM_TEXT_COLOR_GREEN blue:TABBAR_ITEM_TEXT_COLOR_BLUE alpha:1.0f], NSForegroundColorAttributeName,nil] forState:UIControlStateNormal];
    [item1 setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"HelveticaNeue" size:10.0f], NSFontAttributeName,  [UIColor colorWithRed:TABBAR_ITEM_TEXT_COLOR_RED green:TABBAR_ITEM_TEXT_COLOR_GREEN blue:TABBAR_ITEM_TEXT_COLOR_BLUE alpha:1.0f], NSForegroundColorAttributeName,nil] forState:UIControlStateNormal];
    [item2 setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"HelveticaNeue" size:10.0f], NSFontAttributeName,  [UIColor colorWithRed:TABBAR_ITEM_TEXT_COLOR_RED green:TABBAR_ITEM_TEXT_COLOR_GREEN blue:TABBAR_ITEM_TEXT_COLOR_BLUE alpha:1.0f], NSForegroundColorAttributeName,nil] forState:UIControlStateNormal];
    [item3 setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"HelveticaNeue" size:10.0f], NSFontAttributeName,  [UIColor colorWithRed:TABBAR_ITEM_TEXT_COLOR_RED green:TABBAR_ITEM_TEXT_COLOR_GREEN blue:TABBAR_ITEM_TEXT_COLOR_BLUE alpha:1.0f], NSForegroundColorAttributeName,nil] forState:UIControlStateNormal];
    
    [item0 setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"HelveticaNeue" size:10.0f], NSFontAttributeName,  [UIColor colorWithRed:TABBAR_ITEM_SELECTED_TEXT_COLOR_RED green:TABBAR_ITEM_SELECTED_TEXT_COLOR_GREEN blue:TABBAR_ITEM_SELECTED_TEXT_COLOR_BLUE alpha:1.0f], NSForegroundColorAttributeName,nil] forState:UIControlStateHighlighted];
    [item1 setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"HelveticaNeue" size:10.0f], NSFontAttributeName,  [UIColor colorWithRed:TABBAR_ITEM_SELECTED_TEXT_COLOR_RED green:TABBAR_ITEM_SELECTED_TEXT_COLOR_GREEN blue:TABBAR_ITEM_SELECTED_TEXT_COLOR_BLUE alpha:1.0f], NSForegroundColorAttributeName,nil] forState:UIControlStateHighlighted];
    [item2 setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"HelveticaNeue" size:10.0f], NSFontAttributeName,  [UIColor colorWithRed:TABBAR_ITEM_SELECTED_TEXT_COLOR_RED green:TABBAR_ITEM_SELECTED_TEXT_COLOR_GREEN blue:TABBAR_ITEM_SELECTED_TEXT_COLOR_BLUE alpha:1.0f], NSForegroundColorAttributeName,nil] forState:UIControlStateHighlighted];
    [item3 setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"HelveticaNeue" size:10.0f], NSFontAttributeName,  [UIColor colorWithRed:TABBAR_ITEM_SELECTED_TEXT_COLOR_RED green:TABBAR_ITEM_SELECTED_TEXT_COLOR_GREEN blue:TABBAR_ITEM_SELECTED_TEXT_COLOR_BLUE alpha:1.0f], NSForegroundColorAttributeName,nil] forState:UIControlStateHighlighted];
}
- (void)interstitialDidReceiveAd:(DFPInterstitial *)ad
{
    [interstitialAd presentFromRootViewController:self];
}

@end
