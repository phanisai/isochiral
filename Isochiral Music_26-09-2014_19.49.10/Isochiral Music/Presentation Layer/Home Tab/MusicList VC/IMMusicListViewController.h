//
//  IMMusicListViewController.h
//  Isochiral Music
//
//  Created by Sripad on 23/01/14.
//  Copyright (c) 2014 MobileWays. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMBaseViewController.h"
#import "IMLoginViewController.h"
#import "GADBannerView.h"
@class GADBannerView;
@interface IMMusicListViewController : IMBaseViewController <IMLoginDelegate,GADBannerViewDelegate,GADInterstitialDelegate>
@property (weak, nonatomic) IBOutlet UITableView *musicList;
@property (weak, nonatomic) IBOutlet UISearchBar *musicSearch;

@property (assign, nonatomic) BOOL isLoginViewShown;

@property (nonatomic,strong) GADInterstitial *interstitialAd;
@property (strong,nonatomic) IBOutlet GADBannerView *addView;
@end

