//
//  IMLoginViewController.m
//  Isochiral Music
//
//  Created by Sripad on 23/01/14.
//  Copyright (c) 2014 MobileWays. All rights reserved.
//

#import "IMLoginViewController.h"
#import "IMMusicListViewController.h"
#import "IMPlaylistViewController.h"
#import "IMAboutViewController.h"
#import "DFPInterstitial.h"

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

@interface IMLoginViewController () {
    UITabBarController *myTabBarController;
    UITextField *activeTextField;
}

@end

@implementation IMLoginViewController
@synthesize interstitialAd;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        //        [self.navigationController setNavigationBarHidden:YES];
        //        self.tabBarController.tabBar.hidden = YES;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    activeTextField = self.loginEmail;
    self.registerScroll.frame = CGRectMake(320, 0, 320, IPHONE_SCREEN_HEIGHT);
    self.forgortPasswordScroll.frame = CGRectMake(320, 0, 320, IPHONE_SCREEN_HEIGHT);
    for (UIView *view in [self.view subviews]) {
        if ([view isKindOfClass:[UIScrollView class]]) {
            UIScrollView *scroll = (UIScrollView *)view;
            for (UIView *view2 in [scroll subviews]) {
                if ([view2 isKindOfClass:[UITextField class]]) {
                    UITextField *textField = (UITextField *)view2;
                    [textField setValue:[UIColor colorWithWhite:0.5 alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
                    int margin = 0;
                    if ([[textField.placeholder lowercaseString]  isEqualToString: @"password"]) {
                        margin = 10;
                    }
                    else if ([[textField.placeholder lowercaseString] isEqualToString:@"confirm password" ]) {
                        margin = 20;
                    }
                    textField.frame = CGRectMake(textField.frame.origin.x, textField.frame.origin.y+margin, textField.frame.size.width, 40);
                }
                else if ([view2 isKindOfClass:[UIButton class]]) {
                    UIButton *button = (UIButton *)view2;
                    button.layer.cornerRadius = 5.0f;
                }
            }
        }
    }
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        self.loginEmail.frame = CGRectMake(20, 50, 280, 40);
        self.loginPassword.frame = CGRectMake(20, 100, 280, 40);
        self.registerEmail.frame = CGRectMake(20, 50, 280, 40);
        self.registerPassword.frame = CGRectMake(20, 100, 280, 40);
        self.registerConfirmPassword.frame = CGRectMake(20, 150, 280, 40);
        self.forgotPasswordEmail.frame = CGRectMake(20, 50, 280, 40);
    }
    self.skipLogin.layer.cornerRadius = 5.0f;
    self.skipRegister.layer.cornerRadius = 5.0f;
    self.login.frame = CGRectMake(165, self.loginPassword.frame.origin.y+75, 135, 40);
    self.signUp.frame = CGRectMake(20, self.loginPassword.frame.origin.y+75, 135, 40);
    self.forgotPassword.frame = CGRectMake(165, self.loginPassword.frame.origin.y+40, 135, 25);
    self.registerCancel.frame = CGRectMake(20, self.registerConfirmPassword.frame.origin.y+50, 135, 40);
    self.registerSignUp.frame = CGRectMake(165, self.registerConfirmPassword.frame.origin.y+50, 135, 40);
    self.forgortPasswordCancel.frame = CGRectMake(20, self.forgotPasswordEmail.frame.origin.y+50, 135, 40);
    self.forgortPasswordDone.frame = CGRectMake(165, self.forgotPasswordEmail.frame.origin.y+50, 135, 40);
    if ([PFUser currentUser]) {
        IMMusicListViewController *musicListVC = [[IMMusicListViewController alloc] initWithNibName:@"IMMusicListViewController" bundle:nil];
        [self.navigationController pushViewController:musicListVC animated:YES];
    }



    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //    [self.navigationController setNavigationBarHidden:YES];
    //    self.tabBarController.tabBar.hidden = YES;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)login:(id)sender {
    [activeTextField resignFirstResponder];
    if (self.loginEmail.text.length == 0 || self.loginPassword.text.length == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Fill all fields" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    else {
        [super showPlainViewWithFrame:CGRectMake(0, 0, 320, [UIScreen mainScreen].bounds.size.height)];
        [PFUser logInWithUsernameInBackground:self.loginEmail.text password:self.loginPassword.text
                                        block:^(PFUser *user, NSError *error) {
                                            [super removePlainView];
                                            if (user) {

                                                if([[NSUserDefaults standardUserDefaults]objectForKey:@"subscriptionEndDate"])
                                                {
                                                    NSDate *localSubscriptionEndDate = [[NSUserDefaults standardUserDefaults]objectForKey:@"subscriptionEndDate"];
                                                    [[PFUser currentUser] setObject:localSubscriptionEndDate forKey:@"subscriptionEndDate"];
                                                    [[PFUser currentUser] saveInBackground];
                                                }
                                                else
                                                {
                                                  //  [[PFUser currentUser] removeObjectForKey:@"subscriptionEndDate"];
                                                    
                                                    //[[PFUser currentUser] saveInBackground];
                                                    
                                                }
                                                [self.delegate userLoggedInSuccessfully:YES];
                                                [self dismissViewControllerAnimated:YES completion:nil];
                                                
                                            } else {
                                                NSString *errorString = [error userInfo][@"error"];
                                                if ([errorString rangeOfString:@"-1009"].location != NSNotFound) {
                                                    errorString = @"There is an error. You must have valid internet connection to register.";
                                                }
                                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:errorString delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                                [alert show];
                                            }
                                        }];
    }
 
    
}
- (IBAction)signUp:(id)sender {
    [activeTextField resignFirstResponder];
    [UIView animateWithDuration:0.8 animations:^{
        self.loginScroll.frame = CGRectMake(-320, 0, 320, IPHONE_SCREEN_HEIGHT);
        self.registerScroll.frame = CGRectMake(0, 0, 320, IPHONE_SCREEN_HEIGHT);
    }];
}
- (void)textFieldDidEndEditing:(UITextField *)textField {
    [textField resignFirstResponder];
    [self.loginScroll setContentOffset:CGPointMake(0, 0) animated:YES];
    [self.loginScroll setContentSize:CGSizeMake(0, 0)];
}
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    activeTextField = textField;
}
-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    [self.loginScroll setContentOffset:CGPointMake(0, 0) animated:YES];
    [self.loginScroll setContentSize:CGSizeMake(0, 0)];
    return YES;
}
- (IBAction)forgortPassword:(id)sender {
    [activeTextField resignFirstResponder];
    [UIView animateWithDuration:0.8 animations:^{
        self.loginScroll.frame = CGRectMake(-320, 0, 320, IPHONE_SCREEN_HEIGHT);
        self.forgortPasswordScroll.frame = CGRectMake(0, 0, 320, IPHONE_SCREEN_HEIGHT);
    }];
    
}
- (IBAction)registerSignUp:(id)sender {
    [activeTextField resignFirstResponder];
    if (![self.registerEmail.text isEqualToString:@""] && ![self.registerConfirmPassword.text isEqualToString:@""] && ![self.registerPassword.text isEqualToString:@""]) {
        if ([self.registerConfirmPassword.text isEqualToString:self.registerPassword.text]) {
            PFUser *user = [PFUser user];
            user.username = self.registerEmail.text;
            user.password = self.registerPassword.text;
            user.email = self.registerEmail.text;
            [super showPlainViewWithFrame:CGRectMake(0, 0, 320, [UIScreen mainScreen].bounds.size.height)];
            [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                if (!error) {
                    if ([IMBusinessClass sharedInstance].subscriptionEndDate!=nil) {
                        [[PFUser currentUser] setObject:[IMBusinessClass sharedInstance].subscriptionEndDate forKey:@"subscriptionEndDate"];
                        [[PFUser currentUser] saveInBackground];
                    }
                    else
                    {
                        [[PFUser currentUser] removeObjectForKey:@"subscriptionEndDate"];
                        [[PFUser currentUser] saveInBackground];
                        
                    }
                    [self.delegate userLoggedInSuccessfully:YES];
                    [self dismissViewControllerAnimated:YES completion:nil];
                } else {
                    NSString *errorString = [error userInfo][@"error"];
                    if ([errorString rangeOfString:@"-1009"].location != NSNotFound) {
                        errorString = @"There is an error. You must have valid internet connection to register.";
                    }
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:errorString delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alert show];
                    // Show the errorString somewhere and let the user try again.
                }
                [super removePlainView];
            }];
        }
        else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Password and Confirm Password must be the same" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Fill all fields" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
}
- (IBAction)registerCancel:(id)sender {
    [activeTextField resignFirstResponder];
    [UIView animateWithDuration:0.8 animations:^{
        self.loginScroll.frame = CGRectMake(0, 0, 320, IPHONE_SCREEN_HEIGHT);
        self.registerScroll.frame = CGRectMake(320, 0, 320, IPHONE_SCREEN_HEIGHT);
    }];
}
- (IBAction)forgortPasswordDone:(id)sender {
    [activeTextField resignFirstResponder];
    [super showPlainViewWithFrame:CGRectMake(0, 0, 320, [UIScreen mainScreen].bounds.size.height)];
    [PFUser requestPasswordResetForEmailInBackground:self.forgotPasswordEmail.text block:^(BOOL succeeded, NSError *error) {
        [super removePlainView];
        if (!succeeded) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[NSString stringWithFormat:@"%@",error] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Password Changed!" message:[NSString stringWithFormat:@"A new password is sent to your email."] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            [UIView animateWithDuration:0.8 animations:^{
                self.forgortPasswordScroll.frame = CGRectMake(320, 0, 320, IPHONE_SCREEN_HEIGHT);
                self.loginScroll.frame = CGRectMake(0, 0, 320, IPHONE_SCREEN_HEIGHT);
            }];
        }
    }];
}
- (IBAction)forgortPasswordCancel:(id)sender {
    [activeTextField resignFirstResponder];
    [UIView animateWithDuration:0.8 animations:^{
        self.forgortPasswordScroll.frame = CGRectMake(320, 0, 320, IPHONE_SCREEN_HEIGHT);
        self.loginScroll.frame = CGRectMake(0, 0, 320, IPHONE_SCREEN_HEIGHT);
    }];
}
- (IBAction)skipLogin:(id)sender {
    [activeTextField resignFirstResponder];
    [self.delegate userLoggedInSuccessfully:NO];
       [self dismissViewControllerAnimated:YES completion:nil];
}

@end
