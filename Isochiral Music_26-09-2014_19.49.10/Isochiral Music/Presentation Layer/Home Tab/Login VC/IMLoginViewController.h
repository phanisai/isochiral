//
//  IMLoginViewController.h
//  Isochiral Music
//
//  Created by Sripad on 23/01/14.
//  Copyright (c) 2014 MobileWays. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMBaseViewController.h"
#import "GADInterstitial.h"

@protocol IMLoginDelegate;

@interface IMLoginViewController : IMBaseViewController <GADInterstitialDelegate>

// Login View
@property (weak, nonatomic) IBOutlet UIScrollView *loginScroll;
@property (weak, nonatomic) IBOutlet UITextField *loginEmail;
@property (weak, nonatomic) IBOutlet UITextField *loginPassword;
@property (weak, nonatomic) IBOutlet UIButton *forgotPassword;
@property (weak, nonatomic) IBOutlet UIButton *login;
@property (weak, nonatomic) IBOutlet UIButton *signUp;
@property (weak, nonatomic) IBOutlet UIButton *skipLogin;
- (IBAction)forgortPassword:(id)sender;
- (IBAction)login:(id)sender;
- (IBAction)signUp:(id)sender;
- (IBAction)skipLogin:(id)sender;

// Register View
@property (weak, nonatomic) IBOutlet UIScrollView *registerScroll;
@property (weak, nonatomic) IBOutlet UITextField *registerEmail;
@property (weak, nonatomic) IBOutlet UITextField *registerPassword;
@property (weak, nonatomic) IBOutlet UITextField *registerConfirmPassword;
@property (weak, nonatomic) IBOutlet UIButton *registerSignUp;
@property (weak, nonatomic) IBOutlet UIButton *skipRegister;
@property (weak, nonatomic) IBOutlet UIButton *registerCancel;
- (IBAction)registerSignUp:(id)sender;
- (IBAction)registerCancel:(id)sender;

// Forgot Password View
@property (weak, nonatomic) IBOutlet UIScrollView *forgortPasswordScroll;
@property (weak, nonatomic) IBOutlet UITextField *forgotPasswordEmail;
@property (weak, nonatomic) IBOutlet UIButton *forgortPasswordDone;
@property (weak, nonatomic) IBOutlet UIButton *forgortPasswordCancel;
- (IBAction)forgortPasswordDone:(id)sender;
- (IBAction)forgortPasswordCancel:(id)sender;

@property (nonatomic, unsafe_unretained) id<IMLoginDelegate> delegate;
@property (nonatomic,strong) GADInterstitial *interstitialAd;

@end

@protocol IMLoginDelegate
-(void)userLoggedInSuccessfully:(BOOL)loggedIn;
@end
