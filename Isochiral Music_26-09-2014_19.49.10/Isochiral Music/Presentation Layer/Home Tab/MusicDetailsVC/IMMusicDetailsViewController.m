//
//  IMMusicDetailsViewController.m
//  Isochiral Music
//
//  Created by Sripad on 23/01/14.
//  Copyright (c) 2014 MobileWays. All rights reserved.
//

#import "IMMusicDetailsViewController.h"


@interface IMMusicDetailsViewController () {
    NSTimer *updateTimer;
    NSTimeInterval time;
    AVAudioPlayer *audioPlayer;
    BOOL isLoginSkipped;
}

@end

@implementation IMMusicDetailsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.backGrndView.backgroundColor = [UIColor colorWithRed:CELL_BACKGROUND_RED green:CELL_BACKGROUND_GREEN blue:CELL_BACKGROUND_BLUE alpha:0.5f];
    self.backGrndView.layer.cornerRadius = CORNER_RADIUS;
    [[UIApplication sharedApplication].keyWindow addSubview:self.downloadingView];
    self.downloadingView.hidden = YES;
    if ([[self.selectedMusicDetails objectForKey:@"freeIndicator"] isEqualToString:@"Y"]) {
        [self.download setTitle:@"Download Full Version (Free)" forState:UIControlStateNormal];
        self.playSample.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:15.0f];
    }
    if ([[[[self.selectedMusicDetails objectForKey:@"sampleFileURL"] componentsSeparatedByString:@"/"] lastObject] isEqualToString:@"affsample.mp3"]) {
        [self.playSample setTitle:@"Play Affirmation Intro Sample" forState:UIControlStateNormal];
        self.playSample.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:15.0f];
    }
    else {
        [self.playSample setTitle:@"Play Isochiral Sample" forState:UIControlStateNormal];
    }
    
    self.downloadingView.frame = CGRectMake(0, 0, 320, IPHONE_SCREEN_HEIGHT);
    [self.playPauseBtn setBackgroundImage:[UIImage imageNamed:@"pausesong.png"] forState:UIControlStateNormal];
    self.closeSamplePlayer.layer.cornerRadius = CORNER_RADIUS;
    self.downloadingView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.7f];
    self.downloadingSubView.layer.cornerRadius = 5.0f;
    self.downloadingSubView.layer.borderColor = [[UIColor whiteColor] CGColor];
    self.downloadingSubView.layer.borderWidth = 1.0f;
    self.samplePlayerView.layer.cornerRadius = 5.0f;
    self.samplePlayerView.layer.borderColor = [[UIColor whiteColor] CGColor];
    self.samplePlayerView.layer.borderWidth = 1.0f;
    [self loadImage];
    self.title = self.musicNameLbl.text =  [self.selectedMusicDetails objectForKey:@"musicName"];
    self.musicDescriptionLabel.text = [self.selectedMusicDetails objectForKey:@"musicDescription"];
    CGSize maximumLabelSize = CGSizeMake(self.musicDescriptionLabel.frame.size.width, 1500);
    CGSize expectedLabelSize = [[self.selectedMusicDetails objectForKey:@"musicDescription"] sizeWithFont:self.musicDescriptionLabel.font constrainedToSize:maximumLabelSize lineBreakMode:self.musicDescriptionLabel.lineBreakMode];
    CGRect newFrame = self.musicDescriptionLabel.frame;
    newFrame.size.height = expectedLabelSize.height+20;
    self.musicDescriptionLabel.frame = newFrame;
    self.download.layer.cornerRadius = 5.0f;
    self.playSample.layer.cornerRadius = 5.0f;
    [self.detailsScroll setContentSize:CGSizeMake(0, newFrame.size.height+20)];
    if ([[self.selectedMusicDetails objectForKey:@"freeIndicator"] isEqualToString:@"N"]) {
        [self.download setTitle:@"Download Full Version" forState:UIControlStateNormal];
    }
}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if ([IMBusinessClass sharedInstance].subscriptionEndDate != nil) {
        if ([[IMBusinessClass sharedInstance].subscriptionEndDate compare:[NSDate date]] == NSOrderedAscending) {
            [self addAdmob];
        }
        else {
            for (UIView *subviews in self.view.subviews) {
                
                if([subviews isKindOfClass:[GADBannerView class]])
                    {
                        [self.addView removeFromSuperview];
                        [subviews removeFromSuperview];
                                      
                    }
                
                    }
            if([ [ UIScreen mainScreen ] bounds ].size.height == 568)  {
                self.detailsScroll.frame = CGRectMake(10, 150, 300, 295);//iphone 5
                self.backGrndView.frame = CGRectMake(10, 150, 300, 295);
            }
            else
            {
                self.detailsScroll.frame = CGRectMake(10, 150, 300, 200);//iphone 4
                self.backGrndView.frame = CGRectMake(10, 150, 300, 200);

                
            }
           

        }
        
    }
    else {
        [self addAdmob];
    }
}

-(void)addAdmob{
    self.addView = [[GADBannerView alloc]initWithAdSize:kGADAdSizeBanner origin:CGPointMake(0, VIEW_HEIGHT+10)];
    [self.view addSubview:self.addView];
    self.addView.adUnitID = Add_Unit_Id;//@"ca-app-pub-7038667452523799/9060866068";
	self.addView.rootViewController = self;
	
	GADRequest *request = [GADRequest request];
	// Enable test ads on simulators.

	[self.addView loadRequest:request];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)loadImage {
    AFHTTPRequestOperation *postOperation = [[AFHTTPRequestOperation alloc] initWithRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[self.selectedMusicDetails objectForKey:@"imageURL"]]]];
    postOperation.responseSerializer = [AFImageResponseSerializer serializer];
    [postOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        self.musicIcon.image = responseObject;
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
    }];
    [postOperation start];
}
-(void)downloadTheFile {
    NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *fileName = [[[self.selectedMusicDetails objectForKey:@"fileURL"] componentsSeparatedByString:@"/"] lastObject];
    NSString* filePath = [documentsPath stringByAppendingPathComponent:fileName];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
    if (fileExists) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Already Downloaded!" message:@"This song is already downloaded to your mobile. Go to playlist to listen." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    else {
        NSString *tempFilePath = [documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_x",fileName]];
        self.downloadingView.hidden = NO;
        self.downloadingSubView.hidden = NO;
        self.downloadProgress.text = [NSString stringWithFormat:@"Downloading: (0%%) "];
        self.samplePlayerView.hidden = YES;
        [self.downloadActivityIndicator startAnimating];
        [self.view bringSubviewToFront:self.downloadingView];
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[self.selectedMusicDetails objectForKey:@"fileURL"]]];
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        operation.outputStream = [NSOutputStream outputStreamToFileAtPath:tempFilePath append:NO];
        [operation setShouldExecuteAsBackgroundTaskWithExpirationHandler:^{
            
        }];
        [operation setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead)
         {
             float progress = (float)totalBytesRead / totalBytesExpectedToRead;
             self.downloadProgress.text = [NSString stringWithFormat:@"Downloading: (%.0f%%) ",progress*100];
         }];
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            [IMBusinessClass sharedInstance].isDownloadInProgress = NO;
            
            NSError * err = NULL;
            NSFileManager * fm = [[NSFileManager alloc] init];
            BOOL result = [fm moveItemAtPath:tempFilePath toPath:filePath error:&err];
            if(!result) {
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Download Error!" message:@"You do not have a valid internet connection. Please connect your device to internet to download the music" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
            }
            else {
                
                self.downloadingView.hidden = YES;
                [self.downloadActivityIndicator stopAnimating];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Downloaded Successfully!" message:@"This song is now downloaded to your mobile. Go to playlist to listen." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Download Error!" message:@"You do not have a valid internet connection. Please connect your device to internet to download the music" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            self.downloadingView.hidden = YES;
            [self.downloadActivityIndicator stopAnimating];
            [IMBusinessClass sharedInstance].isDownloadInProgress = NO;
        }];
        [operation start];
        [IMBusinessClass sharedInstance].isDownloadInProgress = YES;
    }
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        [self inAppPurchase];
    }
}
-(void)inAppPurchase {
    
    
    [super showPlainViewWithFrame:CGRectMake(0, 0, 320, [UIScreen mainScreen].bounds.size.height)];
    
    [PFPurchase buyProduct:@"1" block:^(NSError *error) {
        if (!error) {
            [self removePlainView];
            NSString *userEmail = @"Anonymous";
            if ([PFUser currentUser]) {
                userEmail = [PFUser currentUser].email;
            }

            [PFCloud callFunctionInBackground:@"sendEmailFromIOs"
                               withParameters:@{@"userEmail":userEmail, @"deviceOs":@"Apple"}
                                        block:^(NSString *result, NSError *error) {
                                            if (!error) {
                                                // result is @"Hello world!"
                                            }
                                        }];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Subscribed Successfully!" message:@"You have successfully subscribed for our Isochiral Music Monthly Subscription.You can now download and listen to our music for a month." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        else {
            [self removePlainView];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Subscription Error!" message:[NSString stringWithFormat:@"Error %@",error] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        
    }];
}
-(void)showInApp {
    // debugin code
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ssZ";
    
   if ([IMBusinessClass sharedInstance].subscriptionEndDate != nil) {
      NSDate *now = [IMBusinessClass sharedInstance].subscriptionEndDate;
       NSString *formattedDateString = [dateFormatter stringFromDate:now];
       NSLog(@"Subscription enddate :%@",formattedDateString);
   }else{
       NSLog(@"Subscription enddate :nil");
  
   }
   
// ends here
    
    UIAlertView *inAppAlert = [[UIAlertView alloc] initWithTitle:@"Subscription" message:@"You must have an active subscription to access the Full Length recordings. Subscribe Now! You will get one month access to all recordings and new updates for USD $18.99 (we update our content regularly)." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Subscribe", nil];
    inAppAlert.delegate = self;
    [inAppAlert show];
   
}
- (IBAction)downloadFullVersion:(id)sender {
    
    // debugin code
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ssZ";
    
    if ([IMBusinessClass sharedInstance].subscriptionEndDate != nil) {
        NSDate *now = [IMBusinessClass sharedInstance].subscriptionEndDate;
        NSString *formattedDateString = [dateFormatter stringFromDate:now];
        NSLog(@"Subscription enddate :%@",formattedDateString);
    }else{
        NSLog(@"Subscription enddate :nil");
        
    }
    
    
    if (isLoginSkipped) {
        [self checkToDownloadTheFile];
    }
    else {
        if (![PFUser currentUser]) {
            IMLoginViewController *loginVC = [[IMLoginViewController alloc] initWithNibName:@"IMLoginViewController" bundle:nil];
            loginVC.delegate = self;
            self.isLoginViewShown = YES;
            [self presentViewController:loginVC animated:YES completion:nil];
        }
        else {
            [self checkToDownloadTheFile];
        }
    }
}
-(void)checkToDownloadTheFile {
    if ([[self.selectedMusicDetails objectForKey:@"freeIndicator"] isEqualToString:@"Y"]) {
        [self downloadTheFile];
    }
    else {
        if ([IMBusinessClass sharedInstance].subscriptionEndDate == nil) {
            
                [self showInApp];
            }
            else {
                if ([[IMBusinessClass sharedInstance].subscriptionEndDate compare:[NSDate date]] == NSOrderedDescending) {
                    [self downloadTheFile];
                    
                } else if ([[IMBusinessClass sharedInstance].subscriptionEndDate compare:[NSDate date]] == NSOrderedAscending) {
                    [self showInApp];
                    
                } else {
                    [self downloadTheFile];
                }
            }


    }
}
-(void)userLoggedInSuccessfully:(BOOL)loggedIn {
    self.isLoginViewShown = NO;
    isLoginSkipped = YES;
    if (loggedIn) {
        isLoginSkipped = NO;
    }
    else {
        [self checkToDownloadTheFile];
    }
}
- (IBAction)playSample:(id)sender {
    NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *fileName = [[[self.selectedMusicDetails objectForKey:@"sampleFileURL"] componentsSeparatedByString:@"/"] lastObject];
    NSString* sampleFilePath = [documentsPath stringByAppendingPathComponent:fileName];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:sampleFilePath];
    if (fileExists) {
        [self playAudioForFilePath:sampleFilePath];
    }
    else {
        NSString *tempSampleFilePath = [documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_x",fileName]];
        self.downloadingView.hidden = NO;
        self.downloadingSubView.hidden = NO;
        self.samplePlayerView.hidden = YES;
        [self.downloadActivityIndicator startAnimating];
        [[UIApplication sharedApplication].keyWindow bringSubviewToFront:self.downloadingView];
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[self.selectedMusicDetails objectForKey:@"sampleFileURL"]]];
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        operation.outputStream = [NSOutputStream outputStreamToFileAtPath:tempSampleFilePath append:NO];
        [operation setShouldExecuteAsBackgroundTaskWithExpirationHandler:^{
            
        }];
        [operation setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead)
         {
             float progress = (float)totalBytesRead / totalBytesExpectedToRead;
             self.downloadProgress.text = [NSString stringWithFormat:@"Downloading: (%.0f%%) ",progress*100];
         }];
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            [IMBusinessClass sharedInstance].isDownloadInProgress = NO;
            
            NSError * err = NULL;
            NSFileManager * fm = [[NSFileManager alloc] init];
            BOOL result = [fm moveItemAtPath:tempSampleFilePath toPath:sampleFilePath error:&err];
            if(!result) {
                
            }
            else {
                self.downloadingView.hidden = YES;
                [self.downloadActivityIndicator stopAnimating];
                [self playAudioForFilePath:sampleFilePath];
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Download Error!" message:@"You do not have a valid internet connection. Please connect your device to internet to download the music" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            self.downloadingView.hidden = YES;
            [self.downloadActivityIndicator stopAnimating];
            [IMBusinessClass sharedInstance].isDownloadInProgress = NO;
            
        }];
        [operation start];
        [IMBusinessClass sharedInstance].isDownloadInProgress = YES;
    }
    
}
- (IBAction)closeSamplePlayer:(id)sender {
    self.downloadingView.hidden = YES;
    self.duration.text = @"00:00";
    [updateTimer invalidate];
    time = 0;
    [self.playPauseBtn setBackgroundImage:[UIImage imageNamed:@"playMusic.png"] forState:UIControlStateNormal];
    self.playPauseBtn.tag = 1;
    [[IMBusinessClass sharedInstance].sampleAudioPlayer stop];
    [updateTimer invalidate];
    self.samplePlayerProgress.value = 0;
}

- (IBAction)togglePlayPause:(id)sender {
    if (self.playPauseBtn.tag == 0) {
        [updateTimer invalidate];
        [self.playPauseBtn setBackgroundImage:[UIImage imageNamed:@"playMusic.png"] forState:UIControlStateNormal];
        self.playPauseBtn.tag = 1;
        [[IMBusinessClass sharedInstance].sampleAudioPlayer pause];
        time = [IMBusinessClass sharedInstance].sampleAudioPlayer.currentTime;
        
    }
    else {
        updateTimer =     [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(updateSeekBar) userInfo:nil repeats:YES];
        [self.playPauseBtn setBackgroundImage:[UIImage imageNamed:@"pausesong.png"] forState:UIControlStateNormal];
        self.playPauseBtn.tag = 0;
        [[IMBusinessClass sharedInstance].sampleAudioPlayer setCurrentTime:time];
        [[IMBusinessClass sharedInstance].sampleAudioPlayer prepareToPlay];
        [[IMBusinessClass sharedInstance].sampleAudioPlayer play];
    }
}


- (void)updateSeekBar{
    float progress = [IMBusinessClass sharedInstance].sampleAudioPlayer.currentTime;
    int min = progress/60;
    int sec = (int)progress%60;
    NSString *minString;
    NSString *secString;
    if (min==0) {
        minString = @"00";
    }
    else if (min<10) {
        minString = [NSString stringWithFormat:@"0%d",min];
    }
    else {
        minString = [NSString stringWithFormat:@"%d",min];
    }
    if (sec==0) {
        secString = @"00";
    }
    else if (sec<10) {
        secString = [NSString stringWithFormat:@"0%d",sec];
    }
    else {
        secString = [NSString stringWithFormat:@"%d",sec];
    }
    self.duration.text = [NSString stringWithFormat:@"%@:%@",minString,secString];
    if (progress == self.samplePlayerProgress.maximumValue) {
        [self.playPauseBtn setBackgroundImage:[UIImage imageNamed:@"playMusic.png"] forState:UIControlStateNormal];
        self.playPauseBtn.tag = 1;
    }
    [self.samplePlayerProgress setValue:progress];
}
-(void)playAudioForFilePath:(NSString *)filePath {
    if ([[IMBusinessClass sharedInstance].audioPlayer isPlaying]) {
        [[IMBusinessClass sharedInstance].audioPlayer stop];
    }
    self.downloadingView.hidden = NO;
    self.downloadingSubView.hidden = YES;
    self.samplePlayerView.hidden = NO;
    [self.playPauseBtn setBackgroundImage:[UIImage imageNamed:@"pausesong.png"] forState:UIControlStateNormal];
    [self.view bringSubviewToFront:self.downloadingView];
    NSURL *fileURL = [[NSURL alloc] initFileURLWithPath:filePath];
    
    AVAudioPlayer *audio = [[AVAudioPlayer alloc]
                            initWithContentsOfURL:fileURL error:nil];
    
    [IMBusinessClass sharedInstance].sampleAudioPlayer = audio;
    [IMBusinessClass sharedInstance].sampleAudioPlayer.delegate = self;
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
    [[AVAudioSession sharedInstance] setActive: YES error: nil];
    
    self.samplePlayerProgress.minimumValue = 0;
    
    self.samplePlayerProgress.maximumValue = [IMBusinessClass sharedInstance].sampleAudioPlayer.duration;
    
    [[IMBusinessClass sharedInstance].sampleAudioPlayer play];
    
    updateTimer =     [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(updateSeekBar) userInfo:nil repeats:YES];
}

-(void)audioPlayerDidFinishPlaying:
(AVAudioPlayer *)player successfully:(BOOL)flag
{
    self.duration.text = @"00:00";
    [updateTimer invalidate];
    time = 0;
    [self.playPauseBtn setBackgroundImage:[UIImage imageNamed:@"playMusic.png"] forState:UIControlStateNormal];
    self.playPauseBtn.tag = 1;
    [[IMBusinessClass sharedInstance].sampleAudioPlayer stop];
    [updateTimer invalidate];
    self.samplePlayerProgress.value = 0;
}

- (IBAction)updateProgress:(id)sender {
    [IMBusinessClass sharedInstance].sampleAudioPlayer.currentTime = self.samplePlayerProgress.value;
}
@end
