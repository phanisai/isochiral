//
//  IMMusicDetailsViewController.h
//  Isochiral Music
//
//  Created by Sripad on 23/01/14.
//  Copyright (c) 2014 MobileWays. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMBaseViewController.h"
#import "IMLoginViewController.h"
#import "AVFoundation/AVFoundation.h"
#import "GADBannerView.h"
#import "GADRequest.h"

@interface IMMusicDetailsViewController : IMBaseViewController <IMLoginDelegate,AVAudioPlayerDelegate,UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet UIView *backGrndView;
@property (weak, nonatomic) IBOutlet UIScrollView *detailsScroll;
@property (weak, nonatomic) IBOutlet UIButton *playSample;
@property (weak, nonatomic) IBOutlet UIButton *download;
- (IBAction)downloadFullVersion:(id)sender;
- (IBAction)playSample:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *musicIcon;
@property (weak, nonatomic) IBOutlet UILabel *musicDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *musicNameLbl;
@property (weak, nonatomic) IBOutlet UIView *downloadingView;
@property (weak, nonatomic) IBOutlet UIView *downloadingSubView;
@property (weak, nonatomic) IBOutlet UILabel *downloadProgress;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *downloadActivityIndicator;
@property (weak, nonatomic) IBOutlet UIView *samplePlayerView;
@property (weak, nonatomic) IBOutlet UISlider *samplePlayerProgress;
@property (weak, nonatomic) IBOutlet UIButton *playPauseBtn;
@property (weak, nonatomic) IBOutlet UIButton *closeSamplePlayer;
- (IBAction)closeSamplePlayer:(id)sender;
- (IBAction)togglePlayPause:(id)sender;

- (IBAction)updateProgress:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *duration;

@property (nonatomic, strong) NSDictionary *selectedMusicDetails;
@property (assign, nonatomic) BOOL isLoginViewShown;
@property (nonatomic,strong) IBOutlet GADBannerView *addView;
@end
