//
//  IMPlaylistViewController.m
//  Isochiral Music
//
//  Created by Sripad on 24/01/14.
//  Copyright (c) 2014 MobileWays. All rights reserved.
//

#import "IMPlaylistViewController.h"
#import "IMPlayerViewController.h"
#import "GADBannerView.h"
#import "GADRequest.h"

#define CELL_BACKGROUND_RED 72.0/255.0
#define CELL_BACKGROUND_GREEN 140.0/255.0
#define CELL_BACKGROUND_BLUE 120.0/255.0

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

@interface IMPlaylistViewController () {
    NSArray *playListOfSongs;
    UIBarButtonItem *edit;
}

@end

@implementation IMPlaylistViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"Playlist";
  
    edit = [[UIBarButtonItem alloc] initWithTitle:@"Edit" style:UIBarButtonItemStyleBordered target:self action:@selector(editPlaylist:)];
//    self.navigationItem.rightBarButtonItem = edit;
  //    self.addView = [[GADBannerView alloc]initWithAdSize:kGADAdSizeBanner];
  
}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
     self.playlist.frame = CGRectMake(0, 0, 320, VIEW_HEIGHT);
    if ([IMBusinessClass sharedInstance].subscriptionEndDate != nil) {
        if ([[IMBusinessClass sharedInstance].subscriptionEndDate compare:[NSDate date]] != NSOrderedAscending) {
            NSMutableArray *tempPlayList = [[NSMutableArray alloc] init];
            for (int i = 0; i<[[IMBusinessClass sharedInstance].songsList count]; i++) {
                NSDictionary *song = [[IMBusinessClass sharedInstance].songsList objectAtIndex:i];
                NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                NSString *fileName = [[[song objectForKey:@"fileURL"] componentsSeparatedByString:@"/"] lastObject];
                NSString* sampleFilePath = [documentsPath stringByAppendingPathComponent:fileName];
                BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:sampleFilePath];
                if (fileExists) {
                    [tempPlayList addObject:song];
                }
            }
            playListOfSongs = [[NSArray alloc] initWithArray:tempPlayList];
        }
        else {
            [self showOnlyFreeMusic];
        }
    }
    else {
        [self showOnlyFreeMusic];
    }
    if ([IMBusinessClass sharedInstance].audioPlayer.isPlaying) {
        if ([IMBusinessClass sharedInstance].isAlreadyPushed) {
            [IMBusinessClass sharedInstance].isAlreadyPushed = NO;
        }
        else {
            IMPlayerViewController *playerVC = [[IMPlayerViewController alloc] initWithNibName:@"IMPlayerViewController" bundle:nil];
            playerVC.playListOfSongs = playListOfSongs;
            [IMBusinessClass sharedInstance].isAlreadyPushed = YES;
            [self.navigationController pushViewController:playerVC animated:YES];
        }
    }
    [self.playlist reloadData];
 
    if([playListOfSongs count] == 0)
    {
        self.navigationItem.rightBarButtonItem = nil;
    }
    else
    {         self.navigationItem.rightBarButtonItem = edit;
        
    }

    
    if ([IMBusinessClass sharedInstance].subscriptionEndDate != nil) {
        if ([[IMBusinessClass sharedInstance].subscriptionEndDate compare:[NSDate date]] == NSOrderedAscending) {
            [self addAdmob];
        }
        else {
            for(UIView *view in self.view.subviews)
            {
                if([view isKindOfClass:[GADBannerView class]])
                {
                     [self.addView removeFromSuperview];
                    self.playlist.frame = CGRectMake(0, 0, 320, VIEW_HEIGHT+GAD_SIZE_320x50.height+10);
                }
            }
        }
    }
    else {
        [self addAdmob];
    }
}

-(void)addAdmob {
    self.addView = [[GADBannerView alloc]initWithAdSize:kGADAdSizeBanner origin:CGPointMake(0, VIEW_HEIGHT+10)];
    [self.view addSubview:self.addView];
    self.addView.adUnitID = Add_Unit_Id;//@"ca-app-pub-7038667452523799/9060866068";
	self.addView.rootViewController = self;
	
	GADRequest *request = [GADRequest request];
	// Enable test ads on simulators.
    [self.addView loadRequest:request];

}
-(void)showOnlyFreeMusic {
    NSMutableArray *tempPlayList = [[NSMutableArray alloc] init];
    for (int i = 0; i<[[IMBusinessClass sharedInstance].songsList count]; i++) {
        NSDictionary *song = [[IMBusinessClass sharedInstance].songsList objectAtIndex:i];
        if ([[song objectForKey:@"freeIndicator"] isEqualToString:@"Y"]) {
            NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
            NSString *fileName = [[[song objectForKey:@"fileURL"] componentsSeparatedByString:@"/"] lastObject];
            NSString* sampleFilePath = [documentsPath stringByAppendingPathComponent:fileName];
            BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:sampleFilePath];
            if (fileExists) {
                [tempPlayList addObject:song];
            }
        }
    }
    playListOfSongs = [[NSArray alloc] initWithArray:tempPlayList];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)editPlaylist:(id)sender {
    if ([edit.title isEqualToString:@"Edit"]) {
        [edit setTitle:@"Done"];
    }
    else {
        [edit setTitle:@"Edit"];
    }
    if([playListOfSongs count] == 0)
    {
        self.navigationItem.rightBarButtonItem = nil;
    }
    else
    {
        self.navigationItem.rightBarButtonItem = edit;
        
    }

    [self.playlist reloadData];
}
-(void)deleteTheMusicFromPlaylist:(id)sender {
    if ([edit.title isEqualToString:@"Done"]) {
        int tag  = [sender tag];
        NSString *fileName = [[[[playListOfSongs objectAtIndex:tag] objectForKey:@"fileURL"] componentsSeparatedByString:@"/"] lastObject];
        NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString* filePath = [documentsPath stringByAppendingPathComponent:fileName];
        
        NSMutableArray *tempPlaylist = [[NSMutableArray alloc] initWithArray:playListOfSongs];
        [tempPlaylist removeObjectAtIndex:tag];
        playListOfSongs = [[NSArray alloc]initWithArray:tempPlaylist];
        
        if ([IMBusinessClass sharedInstance].audioPlayer.isPlaying) {
            NSString *pathOfFilePlaying = [[[[IMBusinessClass sharedInstance].audioPlayer.url absoluteString] componentsSeparatedByString:@"file://"] lastObject];
            if ([pathOfFilePlaying rangeOfString:filePath].location != NSNotFound) {
                [[IMBusinessClass sharedInstance].audioPlayer stop];
            }
        }
        [self.playlist deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:tag inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
        BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
        if (fileExists) {
            NSError *error;
            [[NSFileManager defaultManager] removeItemAtPath:filePath error: &error];
        }
    }
    else {
        IMPlayerViewController *playerVC = [[IMPlayerViewController alloc] initWithNibName:@"IMPlayerViewController" bundle:nil];
        playerVC.playListOfSongs = playListOfSongs;
        playerVC.selectedIndex = [NSString stringWithFormat:@"%ld",(long)[sender tag]];
       
        [self.navigationController pushViewController:playerVC animated:YES];
    }
    if([playListOfSongs count] == 0)
    {
        self.navigationItem.rightBarButtonItem = nil;
    }
    else
    {
        self.navigationItem.rightBarButtonItem = edit;
        
    }
}
#pragma mark- Table View Delegate Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [playListOfSongs count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"orderQueueCell2";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    for (UIView *view in [cell.contentView subviews]) {
        [view removeFromSuperview];
    }
    
    NSDictionary *songOfRow = [playListOfSongs objectAtIndex:indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    UIView *backGrndView = [[UIView alloc] initWithFrame:CGRectMake(10, 10, 300, 50)];
    backGrndView.backgroundColor = [UIColor colorWithRed:CELL_BACKGROUND_RED green:CELL_BACKGROUND_GREEN blue:CELL_BACKGROUND_BLUE alpha:0.5f];
    backGrndView.layer.cornerRadius = CORNER_RADIUS;
    
    UIImageView *songPic = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
    [self loadImageForIndex:indexPath.row withMusicDetails:songOfRow];
    songPic.tag = 100+indexPath.row;
    [backGrndView addSubview:songPic];
    UILabel *songName = [[UILabel alloc] initWithFrame:CGRectMake(60, 0, 170, 25)];
    songName.text = [songOfRow objectForKey:@"musicName"];
    songName.textColor = [UIColor whiteColor];
    songName.font = [UIFont boldSystemFontOfSize:13.0f];
    songName.backgroundColor = [UIColor clearColor];
    [backGrndView addSubview:songName];
    
    UILabel *songDuration = [[UILabel alloc] initWithFrame:CGRectMake(60, 25, 100, 25)];
    songDuration.text = [songOfRow objectForKey:@"duration"];
    songDuration.textColor = [UIColor colorWithWhite:0.6f alpha:1.0f];
    songDuration.font = [UIFont systemFontOfSize:13.0f];
    songDuration.backgroundColor = [UIColor clearColor];
    [backGrndView addSubview:songDuration];
    
    UIButton *playPic = [UIButton buttonWithType:UIButtonTypeCustom];
    playPic.frame = CGRectMake(240, 0, 50, 50);
    if ([edit.title isEqualToString:@"Done"]) {
        [playPic setBackgroundImage:[UIImage imageNamed:@"newDelete.png"] forState:UIControlStateNormal];
    }
    else {
        if ([IMBusinessClass sharedInstance].audioPlayer.isPlaying) {
            NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
            NSString *pathOfFilePlaying = [[[[IMBusinessClass sharedInstance].audioPlayer.url absoluteString] componentsSeparatedByString:@"file://"] lastObject];
            NSString *fileName = [[[songOfRow objectForKey:@"fileURL"] componentsSeparatedByString:@"/"] lastObject];
            NSString* filePath = [documentsPath stringByAppendingPathComponent:fileName];
            if ([pathOfFilePlaying rangeOfString:filePath].location != NSNotFound) {
                [playPic setBackgroundImage:[UIImage imageNamed:@"pausesong.png"] forState:UIControlStateNormal];
            }
            else {
                [playPic setBackgroundImage:[UIImage imageNamed:@"playMusic.png"] forState:UIControlStateNormal];
            }
        }
        else {
            [playPic setBackgroundImage:[UIImage imageNamed:@"playMusic.png"] forState:UIControlStateNormal];
        }
    }
    playPic.tag = indexPath.row;
    [playPic addTarget:self action:@selector(deleteTheMusicFromPlaylist:) forControlEvents:UIControlEventTouchUpInside];
    [backGrndView addSubview:playPic];
    
    [cell.contentView addSubview:backGrndView];
    [cell.contentView setBackgroundColor:[UIColor clearColor]];
    [cell setBackgroundColor:[UIColor clearColor]];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    IMPlayerViewController *playerVC = [[IMPlayerViewController alloc] initWithNibName:@"IMPlayerViewController" bundle:nil];
    playerVC.playListOfSongs = playListOfSongs;
    playerVC.selectedIndex = [NSString stringWithFormat:@"%ld",(long)indexPath.row];
   
    [self.navigationController pushViewController:playerVC animated:YES];
}

-(void)loadImageForIndex:(int)row withMusicDetails:(NSDictionary *)musicDetails {
    AFHTTPRequestOperation *postOperation = [[AFHTTPRequestOperation alloc] initWithRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[musicDetails objectForKey:@"imageURL"]]]];
    postOperation.responseSerializer = [AFImageResponseSerializer serializer];
    [postOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        UITableViewCell *cell = [self.playlist cellForRowAtIndexPath:[NSIndexPath indexPathForRow:row inSection:0]];
        for(UIView *view in [cell.contentView subviews]) {
            for (UIView *view2 in [view subviews]) {
                if ([view2 isKindOfClass:[UIImageView class]] && view2.tag == 100+row) {
                    UIImageView *imageView = (UIImageView *)view2;
                    imageView.image = responseObject;
                }
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
    }];
    [postOperation start];
}

@end
