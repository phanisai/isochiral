//
//  IMPlaylistViewController.h
//  Isochiral Music
//
//  Created by Sripad on 24/01/14.
//  Copyright (c) 2014 MobileWays. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMBaseViewController.h"
#import "GADBannerView.h"
#import "GADRequest.h"
@class GADBannerView;
@interface IMPlaylistViewController : IMBaseViewController
@property (weak, nonatomic) IBOutlet UITableView *playlist;
@property (strong, nonatomic) IBOutlet UITableView *backgroundImagView;

@property (strong, nonatomic) IBOutlet GADBannerView *addView;
@end
