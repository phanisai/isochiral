//
//  IMPlayerViewController.m
//  Isochiral Music
//
//  Created by Sripad on 24/01/14.
//  Copyright (c) 2014 MobileWays. All rights reserved.
//

#import "IMPlayerViewController.h"


@interface IMPlayerViewController () {
    NSTimer *updateTimer;
    NSTimeInterval time;
    NSDictionary *songToPlayDetails;
}

@end

@implementation IMPlayerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillDisappear:(BOOL)animated {
    if ([self isMovingFromParentViewController] && ![IMBusinessClass sharedInstance].audioPlayer.isPlaying) {
        [[IMBusinessClass sharedInstance].audioPlayer stop];
        [IMBusinessClass sharedInstance].audioPlayer = nil;
    }
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
    [IMBusinessClass sharedInstance].isAlreadyPushed = YES;
    self.playerView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.8];
    
    if ([IMBusinessClass sharedInstance].audioPlayer.isPlaying) {
        NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *pathOfFilePlaying = [[[[IMBusinessClass sharedInstance].audioPlayer.url absoluteString] componentsSeparatedByString:@"file://"] lastObject];
        if (self.selectedIndex.length != 0) {
            songToPlayDetails = [self.playListOfSongs objectAtIndex:[self.selectedIndex intValue]];
            NSString *fileName = [[[songToPlayDetails objectForKey:@"fileURL"] componentsSeparatedByString:@"/"] lastObject];
            NSString* filePath = [documentsPath stringByAppendingPathComponent:fileName];
            if ([pathOfFilePlaying rangeOfString:filePath].location != NSNotFound) {
                self.songProgress.minimumValue = 0;
                self.songProgress.maximumValue = [IMBusinessClass sharedInstance].audioPlayer.duration;
                [self updateSeekBar];
                updateTimer =     [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(updateSeekBar) userInfo:nil repeats:YES];
            }
            else {
                self.duration.text = [NSString stringWithFormat:@"00:00:00/%@",[songToPlayDetails objectForKey:@"duration"]];
                [self playAudio];
            }
        }
        else {
            for (int i = 0; i<[self.playListOfSongs count]; i++) {
                NSDictionary *song = [self.playListOfSongs objectAtIndex:i];
                NSString *fileName = [[[song objectForKey:@"fileURL"] componentsSeparatedByString:@"/"] lastObject];
                NSString* filePath = [documentsPath stringByAppendingPathComponent:fileName];
                
                if ([pathOfFilePlaying isEqualToString:filePath]) {
                    songToPlayDetails = song;
                    self.selectedIndex = [NSString stringWithFormat:@"%d",i];
                    break;
                }
            }
        }
        if ([self.selectedIndex intValue] == [self.playListOfSongs count]-1) {
            self.playNext.hidden = YES;
        }
        else {
            self.playNext.hidden = NO;
        }
        if ([self.selectedIndex intValue] == 0) {
            self.playPrevious.hidden = YES;
        }
        else {
            self.playPrevious.hidden = NO;
        }
        self.songProgress.minimumValue = 0;
        self.songProgress.maximumValue = [IMBusinessClass sharedInstance].audioPlayer.duration;
        [self updateSeekBar];
        updateTimer =     [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(updateSeekBar) userInfo:nil repeats:YES];
    }
    else {
        songToPlayDetails = [self.playListOfSongs objectAtIndex:[self.selectedIndex intValue]];
        self.duration.text = [NSString stringWithFormat:@"00:00:00/%@",[songToPlayDetails objectForKey:@"duration"]];
        [self playAudio];
    }
    self.title = [songToPlayDetails objectForKey:@"musicName"];
    [self loadImage];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)loadImage {
    AFHTTPRequestOperation *postOperation = [[AFHTTPRequestOperation alloc] initWithRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[songToPlayDetails objectForKey:@"imageURL"]]]];
    postOperation.responseSerializer = [AFImageResponseSerializer serializer];
    [postOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        self.musicIcon.image = responseObject;
        [self setupNowPlayingInfoCenter];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
    }];
    [postOperation start];
}

- (void)updateSeekBar{
    float progress = [IMBusinessClass sharedInstance].audioPlayer.currentTime;
    int min = progress/60;
    int sec = (int)progress%60;
    int hour = min/60;
    NSString *minString;
    NSString *secString;
    NSString *hourString;
    if (hour == 0) {
        hourString = @"00";
    }
    else if (hour<10) {
        hourString = [NSString stringWithFormat:@"0%d",hour];
    }
    else {
        hourString = [NSString stringWithFormat:@"%d",hour];
    }
    if (min==0) {
        minString = @"00";
    }
    else if (min<10) {
        minString = [NSString stringWithFormat:@"0%d",min];
    }
    else {
        minString = [NSString stringWithFormat:@"%d",min];
    }
    if (sec==0) {
        secString = @"00";
    }
    else if (sec<10) {
        secString = [NSString stringWithFormat:@"0%d",sec];
    }
    else {
        secString = [NSString stringWithFormat:@"%d",sec];
    }
    self.duration.text = [NSString stringWithFormat:@"%@:%@:%@/%@",hourString,minString,secString,[songToPlayDetails objectForKey:@"duration"]];
    if (progress == self.songProgress.maximumValue) {
        [self.playPause setBackgroundImage:[UIImage imageNamed:@"playMusic.png"] forState:UIControlStateNormal];
        self.playPause.tag = 1;
    }
    [self.songProgress setValue:progress];
}
-(void)playAudio {
    
    if ([self.selectedIndex intValue] == [self.playListOfSongs count]-1) {
        self.playNext.hidden = YES;
    }
    else {
        self.playNext.hidden = NO;
    }
    if ([self.selectedIndex intValue] == 0) {
        self.playPrevious.hidden = YES;
    }
    else {
        self.playPrevious.hidden = NO;
    }
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *fileName = [[[songToPlayDetails objectForKey:@"fileURL"] componentsSeparatedByString:@"/"] lastObject];
    NSString* filePath = [documentsPath stringByAppendingPathComponent:fileName];
    NSURL *fileURL = [[NSURL alloc] initFileURLWithPath:filePath];
    
    AVAudioPlayer *audio = [[AVAudioPlayer alloc]
                            initWithContentsOfURL:fileURL error:nil];
    
    [IMBusinessClass sharedInstance].audioPlayer = audio;
    [IMBusinessClass sharedInstance].audioPlayer.delegate = self;
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
    [[AVAudioSession sharedInstance] setActive: YES error: nil];
    
    self.songProgress.minimumValue = 0;
    
    self.songProgress.maximumValue = [IMBusinessClass sharedInstance].audioPlayer.duration;
    
    [[IMBusinessClass sharedInstance].audioPlayer play];
    
    updateTimer =     [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(updateSeekBar) userInfo:nil repeats:YES];
    
}
- (IBAction)songProgress:(id)sender {
    [IMBusinessClass sharedInstance].audioPlayer.currentTime = self.songProgress.value;
}
- (IBAction)playPause:(id)sender {
    if (self.playPause.tag == 0) {
        [updateTimer invalidate];
        [self.playPause setBackgroundImage:[UIImage imageNamed:@"playMusic.png"] forState:UIControlStateNormal];
        self.playPause.tag = 1;
        [[IMBusinessClass sharedInstance].audioPlayer pause];
        time = [IMBusinessClass sharedInstance].audioPlayer.currentTime;
        
    }
    else {
        updateTimer =     [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(updateSeekBar) userInfo:nil repeats:YES];
        [self.playPause setBackgroundImage:[UIImage imageNamed:@"pausesong.png"] forState:UIControlStateNormal];
        self.playPause.tag = 0;
        [[IMBusinessClass sharedInstance].audioPlayer setCurrentTime:time];
        [[IMBusinessClass sharedInstance].audioPlayer prepareToPlay];
        [[IMBusinessClass sharedInstance].audioPlayer play];
    }
}

- (IBAction)playNext:(id)sender {
    
    [self.playPause setBackgroundImage:[UIImage imageNamed:@"pausesong.png"] forState:UIControlStateNormal];
    int index =[self.selectedIndex intValue];
    index++;
    if (index<[self.playListOfSongs count]) {
        self.selectedIndex = [NSString stringWithFormat:@"%d",index];
        songToPlayDetails = [self.playListOfSongs objectAtIndex:[self.selectedIndex intValue]];
        self.duration.text = [NSString stringWithFormat:@"00:00:00/%@",[songToPlayDetails objectForKey:@"duration"]];
        self.title = [songToPlayDetails objectForKey:@"musicName"];
        [self loadImage];
        [self playAudio];
    }
    
}

- (IBAction)playPrevious:(id)sender {
    [self.playPause setBackgroundImage:[UIImage imageNamed:@"pausesong.png"] forState:UIControlStateNormal];
    int index =[self.selectedIndex intValue];
    index--;
    if (index>=0) {
        self.selectedIndex = [NSString stringWithFormat:@"%d",index];
        songToPlayDetails = [self.playListOfSongs objectAtIndex:[self.selectedIndex intValue]];
        self.duration.text = [NSString stringWithFormat:@"00:00:00/%@",[songToPlayDetails objectForKey:@"duration"]];
        self.title = [songToPlayDetails objectForKey:@"musicName"];
        [self loadImage];
        [self playAudio];
    }
}

-(void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    self.duration.text = [NSString stringWithFormat:@"00:00:00/%@",[songToPlayDetails objectForKey:@"duration"]];
    [updateTimer invalidate];
    time = 0;
    [self.playPause setBackgroundImage:[UIImage imageNamed:@"playMusic.png"] forState:UIControlStateNormal];
    self.playPause.tag = 1;
    [[IMBusinessClass sharedInstance].audioPlayer stop];
    [updateTimer invalidate];
    self.songProgress.value = 0;
    
    if ([self.selectedIndex intValue] < [self.playListOfSongs count]-1) {
        int index =[self.selectedIndex intValue];
        index++;
        self.selectedIndex = [NSString stringWithFormat:@"%d",index];
        songToPlayDetails = [self.playListOfSongs objectAtIndex:[self.selectedIndex intValue]];
        self.duration.text = [NSString stringWithFormat:@"00:00:00/%@",[songToPlayDetails objectForKey:@"duration"]];
        [self loadImage];
        [self playAudio];
        [self updateSeekBar];
        [self.playPause setBackgroundImage:[UIImage imageNamed:@"pausesong.png"] forState:UIControlStateNormal];
    }
}

- (void)setupNowPlayingInfoCenter {
    Class playingInfoCenter = NSClassFromString(@"MPNowPlayingInfoCenter");
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    if (playingInfoCenter) {
        MPMediaItemArtwork *albumArt = [[MPMediaItemArtwork alloc] initWithImage:self.musicIcon.image];
        NSMutableDictionary *songInfo = [[NSMutableDictionary alloc] init];
        [songInfo setObject:[songToPlayDetails objectForKey:@"musicName"] forKey:MPMediaItemPropertyTitle];
        [songInfo setObject:albumArt forKey:MPMediaItemPropertyArtwork];
        NSArray *durationArray = [[songToPlayDetails objectForKey:@"duration"] componentsSeparatedByString:@":"];
        int hours = [[durationArray objectAtIndex:0] intValue];
        int mins = [[durationArray objectAtIndex:1] intValue];
        int secs = [[durationArray objectAtIndex:2] intValue];
        int dur = hours*3600+mins*60+secs;
        NSNumber *duration = [NSNumber numberWithInt:dur];
        [songInfo setObject:duration forKey:MPMediaItemPropertyPlaybackDuration];
        [songInfo setObject:[NSNumber numberWithInt:1] forKey:MPNowPlayingInfoPropertyPlaybackRate];
        
        [[MPNowPlayingInfoCenter defaultCenter] setNowPlayingInfo:songInfo];
    }
}
- (void)remoteControlReceivedWithEvent: (UIEvent *) receivedEvent {
    
    if (receivedEvent.type == UIEventTypeRemoteControl) {
        
        switch (receivedEvent.subtype) {
            case UIEventSubtypeRemoteControlPause:
                [self musicPausePlay:YES];
                break;
            case UIEventSubtypeRemoteControlPlay:
                [self musicPausePlay:NO];
                break;
            case UIEventSubtypeRemoteControlNextTrack:
                [self playNext:nil];
                break;
            case UIEventSubtypeRemoteControlPreviousTrack:
                [self playPrevious:nil];
                break;
            default: break;
        }
    }
}
- (void)musicPausePlay:(BOOL)isForPause {
    
    if (isForPause){
        [[IMBusinessClass sharedInstance].audioPlayer pause];
    } else  {
        
        [[IMBusinessClass sharedInstance].audioPlayer play];
    }
}

@end
