//
//  IMPlayerViewController.h
//  Isochiral Music
//
//  Created by Sripad on 24/01/14.
//  Copyright (c) 2014 MobileWays. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMBaseViewController.h"
#import <MediaPlayer/MediaPlayer.h>
#import <MediaPlayer/MPNowPlayingInfoCenter.h>

@interface IMPlayerViewController : IMBaseViewController <AVAudioPlayerDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *musicIcon;
@property (weak, nonatomic) IBOutlet UIView *playerView;


@property (weak, nonatomic) IBOutlet UISlider *songProgress;
- (IBAction)songProgress:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *playPause;
- (IBAction)playPause:(id)sender;
- (IBAction)playNext:(id)sender;
- (IBAction)playPrevious:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *playPrevious;
@property (weak, nonatomic) IBOutlet UIButton *playNext;

@property (weak, nonatomic) IBOutlet UILabel *duration;
@property (nonatomic, strong) NSArray *playListOfSongs;
@property (nonatomic, strong) NSString *selectedIndex;
@end
