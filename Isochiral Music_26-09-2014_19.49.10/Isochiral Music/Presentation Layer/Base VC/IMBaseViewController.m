//
//  JABaseViewController.m
//  jamoonAdmin
//
//  Created by Sripad on 27/10/13.
//  Copyright (c) 2013 MobileWays. All rights reserved.
//

#import "IMBaseViewController.h"

#define NAVIGATION_BAR_COLOR_RED 16.0/255.0
#define NAVIGATION_BAR_COLOR_GREEN 43.0/255.0
#define NAVIGATION_BAR_COLOR_BLUE 49.0/255.0

#define TABBAR_COLOR_RED 16.0/255.0
#define TABBAR_COLOR_GREEN 43.0/255.0
#define TABBAR_COLOR_BLUE 49.0/255.0

#define CELL_BACKGROUND_RED 72.0/255.0
#define CELL_BACKGROUND_GREEN 140.0/255.0
#define CELL_BACKGROUND_BLUE 120.0/255.0

@interface IMBaseViewController () {
    UIView *plainView;
    UIActivityIndicatorView *acView;
    
}

@end

@implementation IMBaseViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor colorWithWhite:0.7 alpha:1.0]];
    NSDictionary *navbarTitleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                               [UIColor whiteColor],UITextAttributeTextColor, nil];
    
    [[UINavigationBar appearance] setTitleTextAttributes:navbarTitleTextAttributes];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:NAVIGATION_BAR_COLOR_RED green:NAVIGATION_BAR_COLOR_GREEN blue:NAVIGATION_BAR_COLOR_BLUE alpha:1.0f];
       self.tabBarController.tabBar.barTintColor = [UIColor colorWithRed:TABBAR_COLOR_RED green:TABBAR_COLOR_GREEN blue:TABBAR_COLOR_BLUE alpha:1.0f];
    }
    else {
        [self.tabBarController.tabBar setTintColor:[UIColor colorWithRed:TABBAR_COLOR_RED green:TABBAR_COLOR_GREEN blue:TABBAR_COLOR_BLUE alpha:1.0f]];
        self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:NAVIGATION_BAR_COLOR_RED green:NAVIGATION_BAR_COLOR_GREEN blue:NAVIGATION_BAR_COLOR_BLUE alpha:1.0f];
    }


	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)SetTitleViewForView:(NSString *)header {
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 44)];
    titleLabel.text = header;
    titleLabel.font = [UIFont boldSystemFontOfSize:16.0f];
    titleLabel.textColor = [UIColor whiteColor];
    //    titleLabel.textColor = [UIColor colorWithRed:TITLE_COLOR_RED green:TITLE_COLOR_GREEN blue:TITLE_COLOR_BLUE alpha:1.0f];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [titleLabel setBackgroundColor:[UIColor clearColor]];
    self.navigationItem.titleView = titleLabel;
}
-(void)showPlainViewWithFrame:(CGRect)frame {
    if (!self.isPlainViewShown) {
        self.isPlainViewShown = YES;
        plainView = [[UIView alloc] initWithFrame:frame];
        [plainView setBackgroundColor:[UIColor clearColor]];
        
        UIView *loadingView = [[UIView alloc] initWithFrame:CGRectMake((frame.size.width-120)/2, (frame.size.height-120)/2, 120, 120)];
        [loadingView setBackgroundColor:[UIColor colorWithRed:CELL_BACKGROUND_RED green:CELL_BACKGROUND_GREEN blue:CELL_BACKGROUND_BLUE alpha:1.0f]];
        loadingView.layer.cornerRadius = 5.0f;
        acView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        acView.frame = CGRectMake((loadingView.frame.size.width-50)/2, (loadingView.frame.size.height-50)/2, 50, 50);
        
        // [backgroundView addSubview:activityIndicator];
        //[backgroundView addSubview:loading];
        [loadingView addSubview:acView];
        [acView startAnimating];
        
        UILabel *loading = [[UILabel alloc] initWithFrame:CGRectMake(0, (loadingView.frame.size.height-30), loadingView.frame.size.width, 30)];
        loading.text = @"Loading";
        loading.textAlignment = NSTextAlignmentCenter;
        loading.textColor = [UIColor whiteColor];
        [loading setBackgroundColor:[UIColor clearColor]];
        [loadingView addSubview:loading];
        loadingView.layer.cornerRadius = 3.0f;
        [plainView addSubview:loadingView];
        [[UIApplication sharedApplication].keyWindow addSubview:plainView];
        [[UIApplication sharedApplication].keyWindow bringSubviewToFront:plainView];
        [self.view bringSubviewToFront:plainView];
        
    }
}
-(void)removePlainView {
    self.isPlainViewShown = NO;
    [acView stopAnimating];
    if (plainView) {
        [plainView removeFromSuperview];
    }
}

@end
