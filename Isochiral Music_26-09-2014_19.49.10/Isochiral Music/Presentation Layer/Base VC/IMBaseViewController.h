//
//  JABaseViewController.h
//  jamoonAdmin
//
//  Created by Sripad on 27/10/13.
//  Copyright (c) 2013 MobileWays. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMBusinessClass.h"
#import "GADBannerView.h"
#import "GADRequest.h"

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

#define CELL_BACKGROUND_RED 72.0/255.0
#define CELL_BACKGROUND_GREEN 140.0/255.0
#define CELL_BACKGROUND_BLUE 120.0/255.0

#define CORNER_RADIUS 5.0f
#define LABEL_FONT_NAME @"HelveticaNeue"
#define LABEL_FONT_BOLD @"HelveticaNeue-Bold"
#define LABEL_FONT_LIGHT @"HelveticaNeue-Light"
#define LABEL_FONT_THIN @"HelveticaNeue-Thin"
#define IPHONE_SCREEN_HEIGHT [UIScreen mainScreen].bounds.size.height
#define VIEW_HEIGHT [UIScreen mainScreen].bounds.size.height-self.navigationController.navigationBar.frame.size.height - self.tabBarController.tabBar.frame.size.height-GAD_SIZE_320x50.height-30
#define Add_Unit_Id @"ca-app-pub-3679882451526947/8790496239"

@interface IMBaseViewController : UIViewController

@property (nonatomic, assign) BOOL isPlainViewShown;
//@property(nonatomic,strong) IBOutlet GADBannerView *addView;
-(void)showPlainViewWithFrame:(CGRect)frame;
-(void)removePlainView;
-(void)SetTitleViewForView:(NSString *)header;

@end
