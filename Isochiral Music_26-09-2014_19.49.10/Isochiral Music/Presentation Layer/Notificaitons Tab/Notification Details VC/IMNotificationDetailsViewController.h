//
//  IMNotificationDetailsViewController.h
//  Isochiral Music
//
//  Created by Sripad on 26/02/14.
//  Copyright (c) 2014 MobileWays. All rights reserved.
//

#import "IMBaseViewController.h"

@interface IMNotificationDetailsViewController : IMBaseViewController
@property (weak, nonatomic) IBOutlet UIScrollView *notificationScroll;
@property (weak, nonatomic) IBOutlet UIView *backGrndView;
@property (weak, nonatomic) IBOutlet UIButton *previousNotification;
- (IBAction)previousNotification:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *nextNotification;
- (IBAction)nextNotification:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *notificationText;
@property (weak, nonatomic) IBOutlet UIButton *doneBtn;
- (IBAction)closeNotification:(id)sender;

@property (nonatomic) NSInteger selectedNotificationIndex;

@end
