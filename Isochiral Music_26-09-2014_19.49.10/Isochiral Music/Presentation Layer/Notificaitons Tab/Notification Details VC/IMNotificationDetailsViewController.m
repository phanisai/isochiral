//
//  IMNotificationDetailsViewController.m
//  Isochiral Music
//
//  Created by Sripad on 26/02/14.
//  Copyright (c) 2014 MobileWays. All rights reserved.
//

#import "IMNotificationDetailsViewController.h"

#define LABEL_WIDTH 280
#define LABEL_FRAME_X 10


@interface IMNotificationDetailsViewController () {
    UIFont *labelFont;
    UIColor *buttonLabelColor;
    NSArray *labelArray;
}

@end

@implementation IMNotificationDetailsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    buttonLabelColor = [UIColor colorWithRed:19.0/255.0 green:70.0/255.0 blue:255.0/255.0 alpha:1.0f];
    self.backGrndView.backgroundColor = [UIColor colorWithRed:CELL_BACKGROUND_RED green:CELL_BACKGROUND_GREEN blue:CELL_BACKGROUND_BLUE alpha:0.5f];
    self.backGrndView.layer.cornerRadius = CORNER_RADIUS;
    labelFont = [UIFont fontWithName:@"HelveticaNeue-Light" size:15.0f];
    self.previousNotification.layer.cornerRadius = CORNER_RADIUS;
    self.nextNotification.layer.cornerRadius = CORNER_RADIUS;
    self.doneBtn.layer.cornerRadius = CORNER_RADIUS;
    [self checkForLastOrFirstNotification];
    //    self.notificationText.text = [[[IMBusinessClass sharedInstance].notificationsList objectAtIndex:self.selectedNotificationIndex] objectForKey:@"text"];
    //    self.notificationText.numberOfLines = 0;
    //    [self.notificationText sizeToFit];
    [self prepareView];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)prepareView {
    for (UIView *view in [self.notificationScroll subviews]) {
        [view removeFromSuperview];
    }
    NSString *notificationText = [[[IMBusinessClass sharedInstance].notificationsList objectAtIndex:self.selectedNotificationIndex] objectForKey:@"message"];
    labelArray = [notificationText componentsSeparatedByString:@"<<"];
    NSString *initialLabelString = [labelArray objectAtIndex:0];
    CGSize initialLabelSize = [initialLabelString sizeWithFont:labelFont constrainedToSize:CGSizeMake(LABEL_WIDTH, 2000) lineBreakMode:NSLineBreakByWordWrapping];
    UILabel *initialLabel = [[UILabel alloc] initWithFrame:CGRectMake(LABEL_FRAME_X, 0, LABEL_WIDTH, initialLabelSize.height+5)];
    initialLabel.text = initialLabelString;
    initialLabel.font = labelFont;
    initialLabel.lineBreakMode = NSLineBreakByWordWrapping;
    initialLabel.numberOfLines = 100;
    initialLabel.textColor = [UIColor whiteColor];
    initialLabel.backgroundColor = [UIColor clearColor];
    [self.notificationScroll addSubview:initialLabel];
    float totalHeight = initialLabelSize.height+5;
    for (int i = 1; i<[labelArray count]; i++) {
        NSString *nextString = [NSString stringWithFormat:@"%@",[labelArray objectAtIndex:i]];
        if ([nextString rangeOfString:@">>"].location != NSNotFound) {
            int space = [nextString rangeOfString:@">>"].location;
            NSString *buttonString = [nextString substringToIndex:space];
            NSString *nextLabelString = [nextString substringFromIndex:space+2];
            if (![buttonString isEqualToString:@""]) {
                CGSize buttonLabelSize = [buttonString sizeWithFont:labelFont constrainedToSize:CGSizeMake(LABEL_WIDTH, 2000) lineBreakMode:NSLineBreakByWordWrapping];
                UILabel *buttonLabel = [[UILabel alloc] initWithFrame:CGRectMake(LABEL_FRAME_X, totalHeight, LABEL_WIDTH, buttonLabelSize.height+5)];
                buttonLabel.text = buttonString;
                buttonLabel.font = labelFont;
                buttonLabel.lineBreakMode = NSLineBreakByWordWrapping;
                buttonLabel.numberOfLines = 100;
                buttonLabel.textColor = buttonLabelColor;
                buttonLabel.backgroundColor = [UIColor clearColor];
                [self.notificationScroll addSubview:buttonLabel];
                UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
                [btn addTarget:self action:@selector(linkSelected:) forControlEvents:UIControlEventTouchUpInside];
                btn.backgroundColor = [UIColor clearColor];
                btn.frame = buttonLabel.frame;
                btn.tag = i;
                [self.notificationScroll addSubview:btn];
                totalHeight += buttonLabel.frame.size.height;
            }
            if (![nextLabelString isEqualToString:@""]) {
                CGSize nextLabelSize = [nextLabelString sizeWithFont:labelFont constrainedToSize:CGSizeMake(LABEL_WIDTH, 2000) lineBreakMode:NSLineBreakByWordWrapping];
                UILabel *nextLabel = [[UILabel alloc] initWithFrame:CGRectMake(LABEL_FRAME_X, totalHeight, LABEL_WIDTH, nextLabelSize.height+5)];
                nextLabel.text = nextLabelString;
                nextLabel.font = labelFont;
                nextLabel.lineBreakMode = NSLineBreakByWordWrapping;
                nextLabel.numberOfLines = 100;
                nextLabel.textColor = [UIColor whiteColor];
                nextLabel.backgroundColor = [UIColor clearColor];
                [self.notificationScroll addSubview:nextLabel];
                totalHeight += nextLabel.frame.size.height;
            }
        }
        else {
            CGSize buttonLabelSize = [nextString sizeWithFont:labelFont constrainedToSize:CGSizeMake(LABEL_WIDTH, 2000) lineBreakMode:NSLineBreakByWordWrapping];
            UILabel *buttonLabel = [[UILabel alloc] initWithFrame:CGRectMake(LABEL_FRAME_X, totalHeight, LABEL_WIDTH, buttonLabelSize.height+5)];
            buttonLabel.text = nextString;
            buttonLabel.font = labelFont;
            buttonLabel.textColor = buttonLabelColor;
            buttonLabel.lineBreakMode = NSLineBreakByWordWrapping;
            buttonLabel.backgroundColor = [UIColor clearColor];
            [self.notificationScroll addSubview:buttonLabel];
            UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
            [btn addTarget:self action:@selector(linkSelected:) forControlEvents:UIControlEventTouchUpInside];
            btn.backgroundColor = [UIColor clearColor];
            btn.frame = buttonLabel.frame;
            btn.tag = i;
            [self.notificationScroll addSubview:btn];
            totalHeight += buttonLabel.frame.size.height;
        }
    }
    [self.notificationScroll setContentSize:CGSizeMake(300, totalHeight+5)];
}
-(void)linkSelected:(id)sender {
    int tag = [sender tag];
    NSString *link = @"";
    NSString *wholeString = [NSString stringWithFormat:@"%@",[labelArray objectAtIndex:tag]];
    if ([wholeString rangeOfString:@">>"].location != NSNotFound) {
        link = [[wholeString componentsSeparatedByString:@">>"] firstObject];
    }
    else {
        link = wholeString;
    }
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:link]];
}
-(void)checkForLastOrFirstNotification {
    if (self.selectedNotificationIndex == 0) {
        self.previousNotification.hidden = YES;
    }
    else {
        self.previousNotification.hidden = NO;
    }
    if (self.selectedNotificationIndex == [[IMBusinessClass sharedInstance].notificationsList count]-1) {
        self.nextNotification.hidden = YES;
    }
    else {
        self.nextNotification.hidden = NO;
    }
}
- (IBAction)previousNotification:(id)sender {
    self.selectedNotificationIndex--;
    //    self.notificationText.text = [[[IMBusinessClass sharedInstance].notificationsList objectAtIndex:self.selectedNotificationIndex] objectForKey:@"text"];
    //    self.notificationText.numberOfLines = 0;
    //    [self.notificationText sizeToFit];
    [self prepareView];
    [self checkForLastOrFirstNotification];
    
}
- (IBAction)nextNotification:(id)sender {
    self.selectedNotificationIndex++;
    //    self.notificationText.text = [[[IMBusinessClass sharedInstance].notificationsList objectAtIndex:self.selectedNotificationIndex] objectForKey:@"text"];
    //    self.notificationText.numberOfLines = 0;
    //    [self.notificationText sizeToFit];
    [self prepareView];
    [self checkForLastOrFirstNotification];
    
}
- (IBAction)closeNotification:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
