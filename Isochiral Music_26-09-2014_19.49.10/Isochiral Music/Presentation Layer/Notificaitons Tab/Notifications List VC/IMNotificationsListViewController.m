//
//  IMNotificationsListViewController.m
//  Isochiral Music
//
//  Created by Sripad on 26/02/14.
//  Copyright (c) 2014 MobileWays. All rights reserved.
//

#import "IMNotificationsListViewController.h"
#import "IMNotificationDetailsViewController.h"

#define CELL_BACKGROUND_RED 72.0/255.0
#define CELL_BACKGROUND_GREEN 140.0/255.0
#define CELL_BACKGROUND_BLUE 120.0/255.0

@interface IMNotificationsListViewController () {
    NSDateFormatter *formatNotificationDate;
}

@end

@implementation IMNotificationsListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Notifications";
    formatNotificationDate = [[NSDateFormatter alloc] init];
    [formatNotificationDate setDateFormat:@"dd-MMM-yyyy hh:mm a"];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self getAllMessages];
    self.notificationsListTable.frame = CGRectMake(0, 0, 320, VIEW_HEIGHT);
    if ([IMBusinessClass sharedInstance].subscriptionEndDate != nil) {
        if ([[IMBusinessClass sharedInstance].subscriptionEndDate compare:[NSDate date]] == NSOrderedAscending) {
            [self addAdmob];
        }
        else {
            [self.addView removeFromSuperview];
            self.notificationsListTable.frame = CGRectMake(0, 0, 320, VIEW_HEIGHT+GAD_SIZE_320x50.height+10);
        }
    }
    else {
        [self addAdmob];
    }    
}

-(void)addAdmob{
    self.addView = [[GADBannerView alloc]initWithAdSize:kGADAdSizeBanner origin:CGPointMake(0, VIEW_HEIGHT+10)];
    [self.view addSubview:self.addView];
    self.addView.adUnitID = Add_Unit_Id;//@"ca-app-pub-7038667452523799/9060866068";
	self.addView.rootViewController = self;
	
	GADRequest *request = [GADRequest request];
    [self.addView loadRequest:request];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)getAllMessages {
    [IMBusinessClass sharedInstance].notificationsList = [[NSArray alloc] initWithArray:[[NSUserDefaults standardUserDefaults] valueForKey:@"notificationsList"]];
    [self.notificationsListTable reloadData];
    [super showPlainViewWithFrame:CGRectMake(0, 0, 320, [UIScreen mainScreen].bounds.size.height)];
    PFQuery *query = [PFQuery queryWithClassName:@"Notifications"];
    [query orderByDescending:@"updatedAt"];
    [query whereKey:@"status" equalTo:@"ACTIVE"];
    query.limit = 999;
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            NSMutableArray *tempMessages = [[NSMutableArray alloc] init];
            for (PFObject *object in objects) {
                NSMutableDictionary *message = [[NSMutableDictionary alloc] init];
                [message setValue:[object objectForKey:@"message"] forKey:@"message"];
                [message setValue:object.updatedAt forKey:@"updatedAt"];
                
                [tempMessages addObject:message];
            }
            [super removePlainView];
            [IMBusinessClass sharedInstance].notificationsList = [[NSArray alloc] initWithArray:tempMessages];
            [[NSUserDefaults standardUserDefaults] setValue:[IMBusinessClass sharedInstance].notificationsList forKey:@"notificationsList"];
            [self.notificationsListTable reloadData];
            if ([IMBusinessClass sharedInstance].isFromNotification) {
                [self showNotification];
            }
        }
        else {
            [super removePlainView];
            if ([IMBusinessClass sharedInstance].isFromNotification) {
                [self showNotification];
            }
            // Log details of the failure
            // [self sortObjectsForArray:[IMBusinessClass sharedInstance].songsList];
        }
        [super removePlainView];
    }];
}
-(void)showNotification {
    [IMBusinessClass sharedInstance].isFromNotification = NO;
    if ([[IMBusinessClass sharedInstance].notificationsList count]>0) {
        IMNotificationDetailsViewController *notificationDetails = [[IMNotificationDetailsViewController alloc] initWithNibName:@"IMNotificationDetailsViewController" bundle:nil];
        notificationDetails.selectedNotificationIndex = 0;
        [self presentViewController:notificationDetails animated:YES completion:nil];
    }
}

#pragma mark- Table View Delegate Methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[IMBusinessClass sharedInstance].notificationsList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"orderQueueCell2";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    for (UIView *view in [cell.contentView subviews]) {
        [view removeFromSuperview];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    UIView *backGrndView = [[UIView alloc] initWithFrame:CGRectMake(10, 5, 300, 50)];
    backGrndView.backgroundColor = [UIColor colorWithRed:CELL_BACKGROUND_RED green:CELL_BACKGROUND_GREEN blue:CELL_BACKGROUND_BLUE alpha:0.5f];
    backGrndView.layer.cornerRadius = CORNER_RADIUS;
    
    UILabel *notification = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 270, 25)];
    NSString *notificationText = [[[[IMBusinessClass sharedInstance].notificationsList objectAtIndex:indexPath.row] objectForKey:@"message"] stringByReplacingOccurrencesOfString:@">>" withString:@" "];
    notificationText = [notificationText stringByReplacingOccurrencesOfString:@"<<" withString:@" "];
    notification.text = notificationText;
    notification.textColor = [UIColor whiteColor];
    notification.font = [UIFont systemFontOfSize:13.0f];
    notification.backgroundColor = [UIColor clearColor];
    [backGrndView addSubview:notification];
    
    UILabel *date = [[UILabel alloc] initWithFrame:CGRectMake(10, 25, 270, 25)];
    NSString *dateString = [formatNotificationDate stringFromDate:[[[IMBusinessClass sharedInstance].notificationsList objectAtIndex:indexPath.row] objectForKey:@"updatedAt"]];
    date.text = dateString;
    date.textColor = [UIColor colorWithWhite:0.6f alpha:1.0f];
    date.font = [UIFont systemFontOfSize:13.0f];
    date.backgroundColor = [UIColor clearColor];
    [backGrndView addSubview:date];
    
    [cell.contentView addSubview:backGrndView];
    [cell.contentView setBackgroundColor:[UIColor clearColor]];
    [cell setBackgroundColor:[UIColor clearColor]];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    IMNotificationDetailsViewController *notificationDetails = [[IMNotificationDetailsViewController alloc] initWithNibName:@"IMNotificationDetailsViewController" bundle:nil];
    notificationDetails.selectedNotificationIndex = indexPath.row;
    [self presentViewController:notificationDetails animated:YES completion:nil];
}

@end
