//
//  IMNotificationsListViewController.h
//  Isochiral Music
//
//  Created by Sripad on 26/02/14.
//  Copyright (c) 2014 MobileWays. All rights reserved.
//

#import "IMBaseViewController.h"
#import "GADBannerView.h"
#import "GADRequest.h"

@interface IMNotificationsListViewController : IMBaseViewController
@property (weak, nonatomic) IBOutlet UITableView *notificationsListTable;
@property (strong,nonatomic) IBOutlet GADBannerView *addView;
-(void)showNotification;
@end
