//
//  IMAboutViewController.h
//  Isochiral Music
//
//  Created by Sripad on 24/01/14.
//  Copyright (c) 2014 MobileWays. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMBaseViewController.h"
#import "GADBannerView.h"
#import "GADRequest.h"

@interface IMAboutViewController : IMBaseViewController
@property (weak, nonatomic) IBOutlet UIScrollView *aboutScroll;
@property (weak, nonatomic) IBOutlet UIButton *isochiralLink;
@property (weak, nonatomic) IBOutlet UIButton *brainwaveLink;
- (IBAction)isochiralLink:(id)sender;
- (IBAction)brainwaveLink:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *backGrndView;
@property (strong,nonatomic) IBOutlet GADBannerView *addView;

@end
