//
//  IMAboutViewController.m
//  Isochiral Music
//
//  Created by Sripad on 24/01/14.
//  Copyright (c) 2014 MobileWays. All rights reserved.
//

#import "IMAboutViewController.h"

@interface IMAboutViewController () {
    NSInteger adMobHeight;
}
@end

@implementation IMAboutViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.backGrndView.backgroundColor = [UIColor colorWithRed:CELL_BACKGROUND_RED green:CELL_BACKGROUND_GREEN blue:CELL_BACKGROUND_BLUE alpha:0.5f];
    self.backGrndView.layer.cornerRadius = CORNER_RADIUS;
    [self.aboutScroll setContentSize:CGSizeMake(280, 400)];
    self.title = @"About";
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if ([IMBusinessClass sharedInstance].subscriptionEndDate != nil) {
        if ([[IMBusinessClass sharedInstance].subscriptionEndDate compare:[NSDate date]] == NSOrderedAscending) {
            [self addAdmob];
            adMobHeight=0;
            
        }
        else {
            [self.addView removeFromSuperview];
            adMobHeight=GAD_SIZE_320x50.height+5;
        }
    }
    else {
        [self addAdmob];
        adMobHeight=0;
    }
    CGRect frame=self.backGrndView.frame;
    frame.size.height=VIEW_HEIGHT-10+adMobHeight;
    self.backGrndView.frame=frame;
    
    frame=self.aboutScroll.frame;
    frame.size.height=VIEW_HEIGHT-20+adMobHeight;
    self.aboutScroll.frame=frame;
}

-(void)addAdmob {
    self.addView = [[GADBannerView alloc]initWithAdSize:kGADAdSizeBanner origin:CGPointMake(0, VIEW_HEIGHT+10)];
    [self.view addSubview:self.addView];
    self.addView.adUnitID = Add_Unit_Id;
	self.addView.rootViewController = self;
    GADRequest *request = [GADRequest request];
	[self.addView loadRequest:request];
}
  - (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)isochiralLink:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.isochiral.com"]];
}

- (IBAction)brainwaveLink:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://brainwaveentrainmentstore.com/customerservice/kb/"]];
}
@end
