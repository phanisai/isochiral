//
//  main.m
//  Isochiral Music
//
//  Created by Sripad on 23/01/14.
//  Copyright (c) 2014 MobileWays. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "IMAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([IMAppDelegate class]));
    }
}
