//
//  IMAppDelegate.h
//  Isochiral Music
//
//  Created by Sripad on 23/01/14.
//  Copyright (c) 2014 MobileWays. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IMAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UITabBarController *tabBarController;
-(NSString*)base64forData:(NSData*)theData;
@end
