//
//  IMAppDelegate.m
//  Isochiral Music
//
//  Created by Sripad on 23/01/14.
//  Copyright (c) 2014 MobileWays. All rights reserved.
//

#import "IMAppDelegate.h"
#import "IMLoginViewController.h"
#import "IMPlaylistViewController.h"
#import "IMAboutViewController.h"
#import "IMMusicListViewController.h"
#import "IMBusinessClass.h"
#import "IMNotificationsListViewController.h"
#import "IMNotificationDetailsViewController.h"
#import "IMMusicDetailsViewController.h"


#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

@implementation IMAppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    [application registerForRemoteNotificationTypes:
     UIRemoteNotificationTypeBadge |
     UIRemoteNotificationTypeAlert |
     UIRemoteNotificationTypeSound];
    [Parse setApplicationId:@"H642ETbE6tswEiszat4d2z8i0PcCLExf8Q3B5jeq"
                  clientKey:@"0PReJ7fAX4y3e6Zb85SxketlkPztqK8ebXRcejDG"];
    self.window.backgroundColor = [UIColor whiteColor];
    
    
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];

    
    [PFPurchase addObserverForProduct:@"1" block:^(SKPaymentTransaction *transaction) {
        // Write business logic that should run once this product is purchased.
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
            NSURL *receiptURL =  [[NSBundle mainBundle] appStoreReceiptURL];
            if ([[NSFileManager defaultManager] fileExistsAtPath:[receiptURL path]]) {
                NSData *receiptData = [NSData dataWithContentsOfURL:receiptURL];
                [self validationOfReceiptWithReceipt:receiptData];
            }
        }
        else {
            [self validationOfReceiptWithReceipt:transaction.transactionReceipt];
        }
    }];
    
   
    
  
    

    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        NSDate *date = [[NSUserDefaults standardUserDefaults] valueForKey:@"subscriptionEndDate"];
        if ([date compare:[NSDate date]] == NSOrderedAscending || date == nil) {
            
            NSURL *receiptURL =  [[NSBundle mainBundle] appStoreReceiptURL];
            if ([[NSFileManager defaultManager] fileExistsAtPath:[receiptURL path]]) {
                NSData *receiptData = [NSData dataWithContentsOfURL:receiptURL];
                [self validationOfReceiptWithReceipt:receiptData];
            }
            else
            {
               
                [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
            }
        }
    }
    
    NSDictionary *notification = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    if (notification)
    {
        [IMBusinessClass sharedInstance].isFromNotification = YES;
    }
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryAmbient error:nil];
    IMMusicListViewController *loginVC = [[IMMusicListViewController alloc] initWithNibName:@"IMMusicListViewController" bundle:nil];
    UINavigationController *loginNavVC = [[UINavigationController alloc] initWithRootViewController:loginVC];
    
    IMPlaylistViewController *playlistVC = [[IMPlaylistViewController alloc] initWithNibName:@"IMPlaylistViewController" bundle:nil];
    UINavigationController *playlistNavVC = [[UINavigationController alloc] initWithRootViewController:playlistVC];
    
    IMAboutViewController *aboutVC = [[IMAboutViewController alloc] initWithNibName:@"IMAboutViewController" bundle:nil];
    UINavigationController *aboutNavVC = [[UINavigationController alloc] initWithRootViewController:aboutVC];
    IMNotificationsListViewController *notificationsListVC = [[IMNotificationsListViewController alloc] initWithNibName:@"IMNotificationsListViewController" bundle:nil];
    UINavigationController *notificationsListNavVC = [[UINavigationController alloc] initWithRootViewController:notificationsListVC];
    self.tabBarController = [[UITabBarController alloc] init];
    self.tabBarController.viewControllers = [[NSArray alloc] initWithObjects:loginNavVC, playlistNavVC, aboutNavVC, notificationsListNavVC, nil];
    [self.window setRootViewController:self.tabBarController];
    [self.window makeKeyAndVisible];
    
    return YES;
}
- (void)application:(UIApplication *)application
didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)newDeviceToken {
    // Store the deviceToken in the current installation and save it to Parse.
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation setDeviceTokenFromData:newDeviceToken];
    [currentInstallation saveInBackground];
}
- (void)application:(UIApplication *)application
didReceiveRemoteNotification:(NSDictionary *)userInfo {
    
    [IMBusinessClass sharedInstance].isFromNotification = YES;
    
    if (application.applicationState == UIApplicationStateActive)
    {
        NSString *message = @"You have recieved a notification.";
        NSString *cancelButtonTitle = @"Check it out";
        if ([IMBusinessClass sharedInstance].isDownloadInProgress) {
            message = @"You have recieved a notification. Check it out after the download is completed";
            cancelButtonTitle = @"OK";
        }
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Notification Recieved!" message:message delegate:self cancelButtonTitle:cancelButtonTitle otherButtonTitles:nil];
        [alertView show];
    }
    else if (application.applicationState == UIApplicationStateBackground || application.applicationState == UIApplicationStateInactive)
    {
        if ([IMBusinessClass sharedInstance].sampleAudioPlayer.isPlaying) {
            [[IMBusinessClass sharedInstance].sampleAudioPlayer stop];
        }
        if (![IMBusinessClass sharedInstance].isDownloadInProgress) {
            if (self.tabBarController.selectedIndex == 3) {
                UINavigationController *notificationListNavVC = (UINavigationController *)self.tabBarController.selectedViewController;
                int numberOfViewControllers  = [notificationListNavVC.viewControllers count];
                if ([[notificationListNavVC.viewControllers objectAtIndex:numberOfViewControllers-1] isKindOfClass:[IMNotificationsListViewController class]]) {
                    IMNotificationsListViewController *notificationListVC = (IMNotificationsListViewController *)[notificationListNavVC.viewControllers objectAtIndex:numberOfViewControllers-1];
                    [notificationListVC showNotification];
                }
            }
            else if (self.tabBarController.selectedIndex == 0) {
                UINavigationController *musicNavVC = (UINavigationController *)self.tabBarController.selectedViewController;
                int numberOfViewControllers  = [musicNavVC.viewControllers count];
                if ([[musicNavVC.viewControllers objectAtIndex:numberOfViewControllers-1] isKindOfClass:[IMMusicListViewController class]]) {
                    IMMusicListViewController *musicListVC = (IMMusicListViewController *)[musicNavVC.viewControllers objectAtIndex:numberOfViewControllers-1];
                    if (musicListVC.isLoginViewShown) {
                        [musicListVC.presentedViewController dismissViewControllerAnimated:YES completion:nil];
                    }
                    
                }
                else if ([[musicNavVC.viewControllers objectAtIndex:numberOfViewControllers-1] isKindOfClass:[IMMusicDetailsViewController class]]) {
                    IMMusicDetailsViewController *musicDetailsVC = (IMMusicDetailsViewController *)[musicNavVC.viewControllers objectAtIndex:numberOfViewControllers-1];
                    if (musicDetailsVC.isLoginViewShown) {
                        [musicDetailsVC.presentedViewController dismissViewControllerAnimated:YES completion:nil];
                    }
                }
                [self.tabBarController setSelectedIndex:3];
            }
            else {
                [self.tabBarController setSelectedIndex:3];
            }
        }
    }
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber: 0];
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        if ([IMBusinessClass sharedInstance].sampleAudioPlayer.isPlaying) {
            [[IMBusinessClass sharedInstance].sampleAudioPlayer stop];
        }
        if (![IMBusinessClass sharedInstance].isDownloadInProgress) {
            if (self.tabBarController.selectedIndex == 3) {
                UINavigationController *notificationListNavVC = (UINavigationController *)self.tabBarController.selectedViewController;
                int numberOfViewControllers  = [notificationListNavVC.viewControllers count];
                if ([[notificationListNavVC.viewControllers objectAtIndex:numberOfViewControllers-1] isKindOfClass:[IMNotificationsListViewController class]]) {
                    IMNotificationsListViewController *notificationListVC = (IMNotificationsListViewController *)[notificationListNavVC.viewControllers objectAtIndex:numberOfViewControllers-1];
                    [notificationListVC showNotification];
                }
            }
            else if (self.tabBarController.selectedIndex == 0) {
                UINavigationController *musicNavVC = (UINavigationController *)self.tabBarController.selectedViewController;
                int numberOfViewControllers  = [musicNavVC.viewControllers count];
                if ([[musicNavVC.viewControllers objectAtIndex:numberOfViewControllers-1] isKindOfClass:[IMMusicListViewController class]]) {
                    IMMusicListViewController *musicListVC = (IMMusicListViewController *)[musicNavVC.viewControllers objectAtIndex:numberOfViewControllers-1];
                    if (musicListVC.isLoginViewShown) {
                        [musicListVC.presentedViewController dismissViewControllerAnimated:YES completion:nil];
                    }
                    
                }
                else if ([[musicNavVC.viewControllers objectAtIndex:numberOfViewControllers-1] isKindOfClass:[IMMusicDetailsViewController class]]) {
                    IMMusicDetailsViewController *musicDetailsVC = (IMMusicDetailsViewController *)[musicNavVC.viewControllers objectAtIndex:numberOfViewControllers-1];
                    if (musicDetailsVC.isLoginViewShown) {
                        [musicDetailsVC.presentedViewController dismissViewControllerAnimated:YES completion:nil];
                    }
                }
                [self.tabBarController setSelectedIndex:3];
            }
            else {
                [self.tabBarController setSelectedIndex:3];
            }
        }
    }
}
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}



-(void)validationOfReceiptWithReceipt:(NSData *)receipt {
    
    
    // Create the JSON object that describes the request
    NSError *error;
    NSDictionary *requestContents;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        requestContents = @{
                            @"receipt-data": [receipt base64EncodedStringWithOptions:0],
                            @"password":@"a6bf1c1e0acb41c59e0a0d350fd8af11"
                            };
    }
    else {
        requestContents = @{
                            @"receipt-data": [self base64forData:receipt],
                            @"password":@"a6bf1c1e0acb41c59e0a0d350fd8af11"
                            };
    }
    
    NSData *requestData = [NSJSONSerialization dataWithJSONObject:requestContents
                                                          options:0
                                                            error:&error];
    
    if (!requestData) { /* ... Handle error ... */ }
    
    // Create a POST request with the receipt data.
    NSURL *storeURL = [NSURL URLWithString:@"https://sandbox.itunes.apple.com/verifyReceipt"];

    //NSURL *storeURL = [NSURL URLWithString:@"https://buy.itunes.apple.com/verifyReceipt"];
    
    NSMutableURLRequest *storeRequest = [NSMutableURLRequest requestWithURL:storeURL];
    [storeRequest setHTTPMethod:@"POST"];
    [storeRequest setHTTPBody:requestData];
    
    // Make a connection to the iTunes Store on a background queue.
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:storeRequest queue:queue
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                               if (connectionError) {
                                   /* ... Handle error ... */
                               } else {
                                   NSError *error;
                                   NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                                   if (!jsonResponse) { NSLog(@"error is %@",error); }
                                   /* ... Send a response back to the device ... */
                        
                                  //NSDictionary *receipt = [jsonResponse objectForKey:@"receipt"];
                                   NSArray *inappArray = [jsonResponse objectForKey:@"latest_receipt_info"];
                                   NSLog(@"recepit Info: %@",inappArray);

                                   
                                   NSArray *sortedArray = [inappArray sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
                                       
                                       
                                       NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                                       [dateFormatter setDateFormat:@"yyyy-mm-dd HH:mm:ss z"];
                                       NSDate *d1 = [dateFormatter dateFromString:obj1[@"expires_date"]];
                                       NSDate *d2 = [dateFormatter dateFromString:obj2[@"expires_date"]];
                                       
                                       return [d1 compare:d2]; // ascending order
                                       
                                   }];
                                   
                                   
                                   inappArray = [sortedArray mutableCopy];
                                   
                                   NSLog(@"lost recipt : %@",[inappArray objectAtIndex:[inappArray count]-1]);
                                   
                                   if ([inappArray count]>0) {
                                       NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                                       [dateFormatter setDateFormat:@"yyyy-mm-dd HH:mm:ss z"];
                                       
                                       NSDictionary *latestReceipt = [inappArray objectAtIndex:[inappArray count]-1];
                                       NSString *recieptNumber = [latestReceipt objectForKey:@"transaction_id"];
                                       NSString *subscriptionPurchaseDate = [latestReceipt objectForKey:@"purchase_date_ms"];
                                       NSTimeInterval interval=[subscriptionPurchaseDate doubleValue]/1000;
                                       NSDate *subscriptionStartDate = [NSDate dateWithTimeIntervalSince1970:interval];
                                       NSString *subscriptionExpiresDate = [latestReceipt objectForKey:@"expires_date_ms"];
                                       interval=[subscriptionExpiresDate doubleValue]/1000;
                                       NSDate *subscriptionEndDate = [NSDate dateWithTimeIntervalSince1970:interval];
                                       NSLog(@"recepit Info: %@",subscriptionEndDate);

                                       [[NSUserDefaults standardUserDefaults] setValue:subscriptionEndDate forKey:@"subscriptionEndDate"];
                                         [IMBusinessClass sharedInstance].subscriptionEndDate = subscriptionEndDate;
                                       
                                       if ([PFUser currentUser]) {
                                           PFUser *currentUser = [PFUser currentUser];
                                           [currentUser setObject:subscriptionStartDate forKey:@"subscriptionStartDate"];
                                           [currentUser setObject:subscriptionEndDate forKey:@"subscriptionEndDate"];
                                           [currentUser setObject:@"INAPP APPLE" forKey:@"subscriptionType"];
                                           [currentUser setObject:recieptNumber forKey:@"receiptNumber"];
                                           [currentUser setObject:@"ACTIVE" forKey:@"status"];
                                           [currentUser saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                                               if (succeeded) {
//                                                [[NSUserDefaults standardUserDefaults]setValue:[[PFUser currentUser] objectForKey:@"subscriptionEndDate"] forKey:@"subscriptionEndDate"];
//                                                   [IMBusinessClass sharedInstance].subscriptionEndDate = [[PFUser currentUser] objectForKey:@"subscriptionEndDate"];
                                            }
                                           }];
                                       }
                                   }
                               }
                           }];
}

-(NSString*)base64forData:(NSData*)theData {
    
    const uint8_t* input = (const uint8_t*)[theData bytes];
    NSInteger length = [theData length];
    
    static char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    
    NSMutableData* data = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    uint8_t* output = (uint8_t*)data.mutableBytes;
    
    NSInteger i;
    for (i=0; i < length; i += 3) {
        NSInteger value = 0;
        NSInteger j;
        for (j = i; j < (i + 3); j++) {
            value <<= 8;
            
            if (j < length) {
                value |= (0xFF & input[j]);
            }
        }
        
        NSInteger theIndex = (i / 3) * 4;
        output[theIndex + 0] =                    table[(value >> 18) & 0x3F];
        output[theIndex + 1] =                    table[(value >> 12) & 0x3F];
        output[theIndex + 2] = (i + 1) < length ? table[(value >> 6)  & 0x3F] : '=';
        output[theIndex + 3] = (i + 2) < length ? table[(value >> 0)  & 0x3F] : '=';
    }
    
    return [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
}
//- (void) paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue
//{
//    
//    NSLog(@"received restored transactions: %i", queue.transactions.count);
//    for (SKPaymentTransaction *transaction in queue.transactions)
//    {
//        NSString *productID = transaction.payment.productIdentifier;
//     
//    }
//}



@end
