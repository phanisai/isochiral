//
//  JABusinessClass.h
//  jamoonAdmin
//
//  Created by Sripad on 25/10/13.
//  Copyright (c) 2013 MobileWays. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AVFoundation/AVFoundation.h"

@interface IMBusinessClass : NSObject

@property (nonatomic,strong) NSDate *subscriptionEndDate;
@property (nonatomic,strong) NSArray *songsList;
@property (nonatomic,strong) NSArray *notificationsList;
@property (nonatomic, strong) AVAudioPlayer *audioPlayer;
@property (nonatomic, strong) AVAudioPlayer *sampleAudioPlayer;
@property (nonatomic,assign) BOOL isAlreadyPushed;
@property (nonatomic,assign) BOOL isFromNotification;
@property (nonatomic,assign) BOOL isDownloadInProgress;
@property (nonatomic,assign) BOOL interstialAdd;;

@property (nonatomic, strong) SKPaymentTransaction *transactionDetails;
@property (nonatomic, strong) NSString *originalTransactionId;
//Singleton instance class

+(IMBusinessClass *)sharedInstance;
@end
