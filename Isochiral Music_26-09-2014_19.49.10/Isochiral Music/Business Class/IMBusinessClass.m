//
//  JABusinessClass.m
//  jamoonAdmin
//
//  Created by Sripad on 25/10/13.
//  Copyright (c) 2013 MobileWays. All rights reserved.
//

#import "IMBusinessClass.h"

@implementation IMBusinessClass
static IMBusinessClass *singletonInstance = nil;

+(IMBusinessClass *) sharedInstance{
    @synchronized(self){
        if (singletonInstance ==nil) {
            singletonInstance = [[IMBusinessClass alloc] init];
        }
        return singletonInstance;
    }
}

- (id)init {
    if (self = [super init]) {
        //Initialize the variables if required here
        
        
    }
    return self;
}

@end
